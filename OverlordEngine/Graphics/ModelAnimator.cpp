//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.113
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "ModelAnimator.h"
#include "../Diagnostics/Logger.h"


ModelAnimator::ModelAnimator(MeshFilter* pMeshFilter) :
m_pMeshFilter(pMeshFilter),
m_Transforms(vector<XMFLOAT4X4>()),
m_IsPlaying(false),
m_Reversed(false),
m_ClipSet(false),
m_TickCount(0),
m_AnimationSpeed(1.0f) {
	SetAnimation(0);
}


ModelAnimator::~ModelAnimator() {
}

void ModelAnimator::SetAnimation(UINT clipNumber) {
	//Set m_ClipSet to false
	m_ClipSet = false;
	//Check if clipNumber is smaller than the actual m_AnimationClips vector size
	if (clipNumber < m_pMeshFilter->m_AnimationClips.size()) {
		//	Retrieve the AnimationClip from the m_AnimationClips vector based on the given clipNumber
		//	Call SetAnimation(AnimationClip clip)
		SetAnimation(m_pMeshFilter->m_AnimationClips.at(clipNumber));
	} else {
		//If not,
		//	Call Reset
		Reset();
		//	Log a warning with an appropriate message
		Logger::LogWarning(L"clipNumber not within boundary!");
		//	return		
	}

}

void ModelAnimator::SetAnimation(wstring clipName) {
	//Set m_ClipSet to false
	m_ClipSet = false;

//	bool succeeded = false;
	//Iterate the m_AnimationClips vector and search for an AnimationClip with the given name (clipName)
	for (auto i : m_pMeshFilter->m_AnimationClips) {
		//If found,
		//	Call SetAnimation(Animation Clip) with the found clip
		if (i.Name == clipName) {
			SetAnimation(i);
//			succeeded = true;
		} else {
			Reset();
			Logger::LogWarning(L"clipName not found!");
		}
	}

	//Else
//	if (!succeeded) {
//		Reset();
//		Logger::LogWarning(L"clipName not found!");
//	}
	//	Call Reset
	//	Log a warning with an appropriate message
}

void ModelAnimator::SetAnimation(AnimationClip clip) {
	//Set m_ClipSet to true
	m_ClipSet = true;
	//Set m_CurrentClip
	m_CurrentClip = clip;
	//Call Reset(false)
	Reset(false);
}

void ModelAnimator::Reset(bool pause) {
	//If pause is true, set m_IsPlaying to false
	if (pause) m_IsPlaying = false;
	//Set m_TickCount to zero
	m_TickCount = 0;
	//Set m_AnimationSpeed to 1.0f
	m_AnimationSpeed = 1.f;
	//If m_ClipSet is true
	if (m_ClipSet) {
		//	Retrieve the BoneTransform from the first Key from the current clip (m_CurrentClip)
		auto boneTransform = m_CurrentClip.Keys.at(0).BoneTransforms;
		//	Refill the m_Transforms vector with the new BoneTransforms (have a look at vector::assign)
		m_Transforms.assign(boneTransform.begin(), boneTransform.end());
	} else {
		//Else
		//	Create an IdentityMatrix 
		XMMATRIX tempMatIdentity = XMMatrixIdentity();
		XMFLOAT4X4 matIdentity;
		XMStoreFloat4x4(&matIdentity, tempMatIdentity);

		//	Refill the m_Transforms vector with this IdenityMatrix (Amount = BoneCount) (have a look at vector::assign)
		m_Transforms.assign(m_pMeshFilter->m_BoneCount, matIdentity);
	}
}

void ModelAnimator::Update(const GameContext& gameContext) {
	//We only update the transforms if the animation is running and the clip is set
	if (m_IsPlaying && m_ClipSet) {
		//1. 
		//Calculate the passedTicks (see the lab document)
		auto passedTicks = gameContext.pGameTime->GetElapsed() * m_CurrentClip.TicksPerSecond * m_AnimationSpeed;
		//Make sure that the passedTicks stay between the m_CurrentClip.Duration bounds (fmod)
		passedTicks = fmod(passedTicks, m_CurrentClip.Duration); //passedTicks%m_CurrentClip.Duration; only for int
		//2. 
		//IF m_Reversed is true
		if (m_Reversed) {
			//	Subtract passedTicks from m_TickCount
			m_TickCount -= passedTicks;
			//	If m_TickCount is smaller than zero, add m_CurrentClip.Duration to m_TickCount
			if (m_TickCount < 0) m_TickCount += m_CurrentClip.Duration;
		} else {
			//ELSE
			//	Add passedTicks to m_TickCount
			m_TickCount += passedTicks;
			//	if m_TickCount is bigger than the clip duration, subtract the duration from m_TickCount
			if (m_TickCount > m_CurrentClip.Duration) m_TickCount -= m_CurrentClip.Duration;
		}

		//3.
		//Find the enclosing keys
		AnimationKey keyA, keyB;
		//Iterate all the keys of the clip and find the following keys:
		for (auto key : m_CurrentClip.Keys) {
			//keyA > Closest Key with Tick before/smaller than m_TickCount
			if (key.Tick <= m_TickCount) {
				keyA = key;
				//keyB > Closest Key with Tick after/bigger than m_TickCount
			} else {
				keyB = key;
				break;
			}
		}

		//4.
		//Interpolate between keys
		float ticksBetweenKeys = (keyB.Tick - keyA.Tick);
		//Figure out the BlendFactor (See lab document)
		float blendFactor = (m_TickCount - keyA.Tick) / ticksBetweenKeys;
		//Clear the m_Transforms vector
		m_Transforms.clear();
		//FOR every boneTransform in a key (So for every bone)
		for (size_t i = 0; i < m_CurrentClip.Keys[i].BoneTransforms.size(); ++i) {
			//	Retrieve the transform from keyA (transformA)
			auto transformA = XMLoadFloat4x4(&keyA.BoneTransforms[i]);
			// 	Retrieve the transform from keyB (transformB)
			auto transformB = XMLoadFloat4x4(&keyB.BoneTransforms[i]);
			//	Decompose both transforms
			XMVECTOR posA, posB, scaleA, scaleB, rotA, rotB;
			XMMatrixDecompose(&scaleA, &rotA, &posA, transformA);
			XMMatrixDecompose(&scaleB, &rotB, &posB, transformB);
			//	Lerp between all the transformations (Position, Scale, Rotation)
			XMVECTOR lerpPosition = XMVectorLerp(posA, posB, blendFactor);
			XMVECTOR lerpScale = XMVectorLerp(scaleA, scaleB, blendFactor);
			XMVECTOR lerpRotation = XMQuaternionSlerp(rotA, rotB, blendFactor);
			//	Compose a transformation matrix with the lerp-results
			XMMATRIX composedMat, scaleMat, rotMat, transMat;
			scaleMat = XMMatrixScalingFromVector(lerpScale);
			rotMat = XMMatrixRotationQuaternion(lerpRotation);
			transMat = XMMatrixTranslationFromVector(lerpPosition);
			composedMat = scaleMat * rotMat * transMat;
			//	Add the resulting matrix to the m_Transforms vector
			XMFLOAT4X4 composedFloat4x4;
			XMStoreFloat4x4(&composedFloat4x4, composedMat);
			m_Transforms.push_back(composedFloat4x4);
		}
	}
}
