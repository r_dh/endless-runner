//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "BoneObject.h"
#include "../Components/Components.h"


BoneObject::BoneObject(int boneId, int materialId, float length) :
	 m_BoneId(boneId),
	 m_MaterialId(materialId),
	 m_Length(length)
{
}


BoneObject::~BoneObject(void)
{
}

void BoneObject::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	GameObject *bone = new GameObject();
	bone->AddComponent(new ModelComponent(L"Resources/Meshes/Bone.ovm"));
	bone->GetComponent<ModelComponent>()->SetMaterial(m_MaterialId);

	bone->GetTransform()->Scale(m_Length, m_Length, m_Length);
	//bone->GetComponent<scale //scale the bone to the proper length (scale all axis)
	AddChild(bone);
}

void BoneObject::AddBone(BoneObject* pBone){
	pBone->GetTransform()->Translate(0.f, 0.f, -m_Length);
	AddChild(pBone);
}

XMFLOAT4X4 BoneObject::GetBindPose(){
	return m_BindPose;
}

void BoneObject::CalculateBindPose(){

	auto world = XMLoadFloat4x4(&GetTransform()->GetWorld());
	auto matinv = XMMatrixInverse(nullptr, world);
	XMStoreFloat4x4(&m_BindPose, matinv);

	for (size_t i = 0; i < GetChildren<BoneObject>().size(); i++) {
		GetChildren<BoneObject>()[i]->CalculateBindPose();
	}

}