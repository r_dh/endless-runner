//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
#include "stdafx.h"
#include "SpriteFontLoader.h"
#include "../Helpers/BinaryReader.h"
#include "../Content/ContentManager.h"
#include "../Graphics/TextRenderer.h"


SpriteFontLoader::SpriteFontLoader()
{

}


SpriteFontLoader::~SpriteFontLoader()
{
}

SpriteFont* SpriteFontLoader::LoadContent(const wstring& assetFile)
{
	auto binReader = new BinaryReader();
	binReader->Open(assetFile);

	if (!binReader->Exists())
	{
		Logger::LogFormat(LogLevel::Error, L"SpriteFontLoader::LoadContent > Failed to read the assetFile!\nPath: \'%s\'", assetFile.c_str());
		return nullptr;
	}

	//See BMFont Documentation for Binary Layout http://www.angelcode.com/products/bmfont/doc/file_format.html

	//Parse the Identification bytes (B,M,F)
	if (binReader->Read<char>() != 'B' || binReader->Read<char>() != 'M' || binReader->Read<char>() != 'F'){
		//If Identification bytes doesn't match B|M|F,
		Logger::LogError(L"Not a valid .fnt font"); //SpriteFontLoader::LoadContent
		return nullptr;
	}

	//Parse the version (version 3 required)
	int version = binReader->Read<char>();
	if (version < 3){ //or read int or unsigned //test
		Logger::LogError(L"Only .fnt version 3 is supported");
		delete binReader;
		return nullptr;
	}
	//If version is < 3,
	//Log Error (SpriteFontLoader::LoadContent > Only .fnt version 3 is supported)
	//return nullptr
	//...

	//Valid .fnt file
	auto pSpriteFont = new SpriteFont();
	//SpriteFontLoader is a friend class of SpriteFont
	//That means you have access to its privates (pSpriteFont->m_FontName = ... is valid)

	//**********
	// BLOCK 0 *
	//**********
	//Retrieve the blockId and blockSize
	int blockId = binReader->Read<char>(); // std::stoi(binReader->ReadString());
	int blockSize = binReader->Read<int>();
	//Retrieve the FontSize (will be -25, BMF bug) [SpriteFont::m_FontSize]	
	int fontSize = binReader->Read<short>();
	if (fontSize > 25) fontSize = 25;
	pSpriteFont->m_FontSize = (short)fontSize;
	//Move the binreader to the start of the FontName [BinaryReader::MoveBufferPosition(...) or you can set its position using BinaryReader::SetBufferPosition(...))
	binReader->MoveBufferPosition(12); //14 is position of fontName http://www.angelcode.com/products/bmfont/doc/file_format.html#bin
	//binReader->SetBufferPosition(14);

	//Retrieve the FontName [SpriteFont::m_FontName]
	pSpriteFont->m_FontName = binReader->ReadNullString();

	//**********
	// BLOCK 1 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = binReader->Read<char>();
	blockSize = binReader->Read<int>();
	//Retrieve Texture Width & Height [SpriteFont::m_TextureWidth/m_TextureHeight]
	////---scaleW @ pos 4 : The width of the texture, normally used to scale the x pos of the character image.
	////---scaleH @ pos 6 : The height of the texture, normally used to scale the y pos of the character image.
	//--width	 @ pos 8+c*20 : The width of the character image in the texture.
	//--The number of characters in the file can be computed by taking the size of the block and dividing with the size of the charInfo structure, i.e.: numChars = charsBlock.blockSize/20
	
	binReader->MoveBufferPosition(4);

	pSpriteFont->m_TextureWidth = binReader->Read<short>(); // std::stoi(binReader->ReadString());
	pSpriteFont->m_TextureHeight = binReader->Read<short>();
	//Retrieve PageCount
	if (binReader->Read<short>() > 1){
		Logger::LogError(L"SpriteFont (.fnt): Only one texture per font allowed");
		delete binReader;
		return nullptr;
	}
	//> if pagecount > 1
	//> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Only one texture per font allowed)
	//Advance to Block2 (Move Reader)
	//...
	binReader->MoveBufferPosition(5); //14 is end with size 1 = 15 for next block

	//**********
	// BLOCK 2 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = binReader->Read<char>();
	blockSize = binReader->Read<int>();
	//Retrieve the PageName (store Local)
	std::wstring pageName = binReader->ReadNullString();
	//	> If PageName is empty
	if (pageName == L""){
		Logger::LogError(L"SpriteFont (.fnt): Invalid Font Sprite [Empty]");
		delete binReader;
		return nullptr;
	}
	//	> Log Error (SpriteFontLoader::LoadContent > SpriteFont (.fnt): Invalid Font Sprite [Empty])
	//>Retrieve texture filepath from the assetFile path
	//> (ex. c:/Example/somefont.fnt => c:/Example/) [Have a look at: wstring::rfind()]
	std::wstring filePath = assetFile;
	std::size_t found = filePath.rfind(pSpriteFont->m_FontName);
	//>Use path and PageName to load the texture using the ContentManager [SpriteFont::m_pTexture]
	filePath = filePath.substr(0, found);
	//> (ex. c:/Example/ + 'PageName' => c:/Example/somefont_0.png)
	//...
	pSpriteFont->m_pTexture = ContentManager::Load<TextureData>(filePath + pageName);

	//**********
	// BLOCK 3 *
	//**********
	//Retrieve the blockId and blockSize
	blockId = binReader->Read<char>();
	blockSize = binReader->Read<int>();
	//Retrieve Character Count (see documentation)
	int numChars = blockSize / 20; //20 because of documentation
	pSpriteFont->m_CharacterCount = numChars;
	//Retrieve Every Character, For every Character:
	for (int i = 0; i < numChars; i++){

		//> Retrieve CharacterId (store Local) and cast to a 'wchar_t'
		wchar_t characterId = (wchar_t)binReader->Read<int>();
		//> Check if CharacterId is valid (SpriteFont::IsCharValid), Log Warning and advance to next character if not valid
		if (!pSpriteFont->IsCharValid(characterId)){
			Logger::LogError(L"SpriteFont (.fnt): Invalid Font Sprite [Empty]");
			continue;
		}
		//> Retrieve the corresponding FontMetric (SpriteFont::GetMetric) [REFERENCE!!!]
		FontMetric& fontMetric = pSpriteFont->GetMetric(characterId); //ADRESS, not copy!!
		//> Set IsValid to true [FontMetric::IsValid]
		fontMetric.IsValid = true;
		//> Set Character (CharacterId) [FontMetric::Character]
		fontMetric.Character = characterId;
		//> Retrieve Xposition (store Local)
		//pSpriteFont->SetBufferPosition()
		int xPos = binReader->Read<short>();
		//> Retrieve Yposition (store Local)
		int yPos = binReader->Read<short>();
		//> Retrieve & Set Width [FontMetric::Width]
		fontMetric.Width = binReader->Read<short>();
		//> Retrieve & Set Height [FontMetric::Height]
		fontMetric.Height = binReader->Read<short>();
		//> Retrieve & Set OffsetX [FontMetric::OffsetX]
		fontMetric.OffsetX = binReader->Read<short>();
		//> Retrieve & Set OffsetY [FontMetric::OffsetY]
		fontMetric.OffsetY = binReader->Read<short>();
		//> Retrieve & Set AdvanceX [FontMetric::AdvanceX]
		fontMetric.AdvanceX = binReader->Read<short>();
		//> Retrieve & Set Page [FontMetric::Page]
		fontMetric.Page = binReader->Read<char>();
		//> Retrieve Channel (BITFIELD!!!) 
		int channel = binReader->Read<char>();
		//	> See documentation for BitField meaning [FontMetrix::Channel]
		if (channel == 1) fontMetric.Channel = 2;
		else if (channel == 2) fontMetric.Channel = 1;
		else if (channel == 4) fontMetric.Channel = 0;
		else if (channel == 8) fontMetric.Channel = 3;
		else if (channel == 15) fontMetric.Channel = 0;
		//bit 0: smooth, bit 1: unicode, bit 2: italic, bit 3: bold, bit 4: fixedHeigth, bits 5-7: reserved
		//The texture channel where the character image is found (1 = blue, 2 = green, 4 = red, 8 = alpha, 15 = all channels).

		//> Calculate Texture Coordinates using Xposition, Yposition, TextureWidth & TextureHeight [FontMetric::TexCoord]
		fontMetric.TexCoord.x = (float)xPos / (float)pSpriteFont->m_TextureWidth;
		fontMetric.TexCoord.y = (float)yPos / (float)pSpriteFont->m_TextureHeight;

	}

	//DONE :)

	delete binReader;
	return pSpriteFont;
}

void SpriteFontLoader::Destroy(SpriteFont* objToDestroy)
{
	SafeDelete(objToDestroy);
}

