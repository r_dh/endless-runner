//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "MainGame.h"
#include "Base\GeneralStructs.h"
#include "Scenegraph\SceneManager.h"
#include "Physx\PhysxProxy.h"
#include "Diagnostics\DebugRenderer.h"
//#include "vld.h"
//CUSTOM SCENES
//Change this define to activate/deactive the corresponding scenes
// W06 - W07 - W08 - W09 - W10 - W11 - W12 (#define ...)
#define W03

#define W11
#define GAME
/*
//#ifdef W03
#include "CourseObjects/Week 3/Pong/PongScene.h"
#include "CourseObjects/Week 3/TutorialScene.h"
//#elif defined(W04)
#include "CourseObjects/Week 4/ModelTestScene.h"
#include "CourseObjects/Week 4/CharacterTest.h"
//#elif defined(W05)
#include "CourseObjects/Week 5/UberScene.h"
#include "CourseObjects/Week 5/SpikeyScene.h"
#include "CourseObjects/Week 5/SpriteTestScene.h"
//#elif defined(W06)
#include "CourseObjects/Week 6/PickScene.h"
#include "CourseObjects/Week 6/SpriteFontTestScene.h"
//#elif defined(W07)
#include "CourseObjects/Week 7/SoftwareSkinScene_1.h"
#include "CourseObjects/Week 7/SoftwareSkinScene_2.h"
#include "CourseObjects/Week 7/SoftwareSkinScene_3.h"
//#elif defined(W08)
#include "CourseObjects/Week 8/HardwareSkinningScene.h"
//#elif defined(W09)
#include "CourseObjects/Week 9/PostProcessingScene.h"
//#elif defined(W10)
#include "CourseObjects/Week 10/ParticleScene.h"
//#elif defined(W11)
#include "CourseObjects/Week 11/SkyBoxScene.h"
//#elif defined(GAME)
#include "CourseObjects/Source/RaceScene.h"*/
#include "CourseObjects/Source/TerrainScene.h"
//#endif

MainGame::MainGame(void)
{
}


MainGame::~MainGame(void)
{
}

//Game is preparing
void MainGame::OnGamePreparing(GameSettings& gameSettings)
{
	UNREFERENCED_PARAMETER(gameSettings);
	//Nothing to do atm.
}

void MainGame::Initialize()
{
	
//#ifdef W03
	/*SceneManager::GetInstance()->AddGameScene(new TutorialScene());
	SceneManager::GetInstance()->AddGameScene(new PongScene());
//#elif defined(W04)
	SceneManager::GetInstance()->AddGameScene(new ModelTestScene());
	SceneManager::GetInstance()->AddGameScene(new CharacterTest());
//#elif defined(W05)
	SceneManager::GetInstance()->AddGameScene(new UberScene());
	SceneManager::GetInstance()->AddGameScene(new SpikeyScene());
	SceneManager::GetInstance()->AddGameScene(new SpriteTestScene());
//#elif defined(W06)
		SceneManager::GetInstance()->AddGameScene(new PickScene());
	SceneManager::GetInstance()->AddGameScene(new SpriteFontTestScene());
//#elif defined(W07)
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinScene_1());
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinScene_2());
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinScene_3());
//#elif defined(W08)
	SceneManager::GetInstance()->AddGameScene(new HardwareSkinningScene());
//#elif defined(W09)
	SceneManager::GetInstance()->AddGameScene(new PostProcessingScene());
//#elif defined(W10)
	SceneManager::GetInstance()->AddGameScene(new ParticleScene());
//#elif defined(W11)
	SceneManager::GetInstance()->AddGameScene(new SkyBoxScene());*/
//#elif defined(GAME)
	//SceneManager::GetInstance()->AddGameScene(new RaceScene());
	SceneManager::GetInstance()->AddGameScene(new TerrainScene());
//#endif

}

LRESULT MainGame::WindowProcedureHook(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hWnd);
	UNREFERENCED_PARAMETER(message);
	UNREFERENCED_PARAMETER(wParam);
	UNREFERENCED_PARAMETER(lParam);

	switch (message)
	{
		case WM_KEYUP:
		{
			if ((lParam & 0x80000000) != 0x80000000)
				return -1;

			//NextScene
			if (wParam == VK_F3)
			{
				SceneManager::GetInstance()->NextScene();
				return 0;
			}
			//PreviousScene
			else if (wParam == VK_F2)
			{
				SceneManager::GetInstance()->PreviousScene();
				return 0;
			}
			else if (wParam == VK_F4)
			{
				DebugRenderer::ToggleDebugRenderer();
				return 0;
			}
			else if (wParam == VK_F6)
			{
				auto activeScene = SceneManager::GetInstance()->GetActiveScene();
				activeScene->GetPhysxProxy()->NextPhysXFrame();
			}
		}
	}

	return -1;
}


