//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "SpriteTestScene.h"
#include "Scenegraph\GameObject.h"
#include "Diagnostics\Logger.h"
#include "Diagnostics\DebugRenderer.h"
#include "Graphics\SpriteRenderer.h"

#include "Content\ContentManager.h"
#include "Graphics\TextureData.h"
#include "Components\SpriteComponent.h"
#include "Components\TransformComponent.h"
#include "Graphics\MeshFilter.h"

#define FPS_COUNTER 1

SpriteTestScene::SpriteTestScene(void) :
GameScene(L"SpriteTestScene"),
m_FpsInterval(FPS_COUNTER),
m_pObj(nullptr)
{
}


SpriteTestScene::~SpriteTestScene(void)
{
}

void SpriteTestScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	////TEST1
	//m_pObj = new GameObject();
	//m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0,0),XMFLOAT4(1,1,1,1.0f)));
	//AddChild(m_pObj);

	////TEST1B
	//m_pObj = new GameObject();
	//m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0, 0), XMFLOAT4(1, 1, 1, 1.0f)));
	//AddChild(m_pObj);
	//m_pObj->GetTransform()->Scale(2.f, 1, 1);

	////TEST2
	//m_pObj = new GameObject();
	//m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0.3f, 0.3f), XMFLOAT4(1, 1, 1, 1.0f)));
	//AddChild(m_pObj);

	////TEST2B
	//m_pObj = new GameObject();
	//m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0.3f, 0.3f), XMFLOAT4(1, 1, 1, 1.0f)));
	//AddChild(m_pObj);
	//m_pObj->GetTransform()->Scale(2.f, 1, 1);

	////TEST3
	//m_pObj = new GameObject();
	//m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0.3f, 0.3f), XMFLOAT4(1, 1, 1, 1.0f)));
	//AddChild(m_pObj);
	//m_pObj->GetTransform()->Rotate(0, 0, 45);

	//TEST4
	m_pObj = new GameObject();
	m_pObj->AddComponent(new SpriteComponent(L"./Resources/Textures/TestSprite.jpg", XMFLOAT2(0.3f, 0.3f), XMFLOAT4(1, 1, 1, 1.0f)));
	AddChild(m_pObj);
	m_pObj->GetTransform()->Translate(640.f, 360.f, .9f);
	m_pObj->GetTransform()->Scale(.5f, 1.f, 1.f);
}

void SpriteTestScene::Update(const GameContext& gameContext)
{
	m_FpsInterval += gameContext.pGameTime->GetElapsed();
	if (m_FpsInterval >= FPS_COUNTER)
	{
		m_FpsInterval -= FPS_COUNTER;
		Logger::LogFormat(LogLevel::Info, L"FPS: %i", gameContext.pGameTime->GetFPS());
	}

	//TEST 4
	m_pObj->GetTransform()->Rotate(0, 0, XM_PIDIV2 * gameContext.pGameTime->GetTotal(), false);
}

void SpriteTestScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
