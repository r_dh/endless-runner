//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "UberScene.h"
#include "Scenegraph\GameObject.h"
#include "Diagnostics\Logger.h"
#include "Diagnostics\DebugRenderer.h"

#include "Prefabs\Prefabs.h"
#include "Components\Components.h"
#include "Physx\PhysxProxy.h"
#include "Physx\PhysxManager.h"
#include "Content\ContentManager.h"
#include "Graphics\MeshFilter.h"
#include "../../Materials/UberMaterial.h"
#include "Components\ModelComponent.h"

#define FPS_COUNTER 1

UberScene::UberScene(void) :
GameScene(L"UberScene"),
m_FpsInterval(FPS_COUNTER)
{
}

UberScene::~UberScene(void)
{
}

void UberScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	GetPhysxProxy()->EnablePhysxDebugRendering(true);
	gameContext.pGameTime->ForceElapsedUpperbound(true);

	// Create PhysX ground plane
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	auto bouncyMaterial = physX->createMaterial(0, 0, 1);
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddChild(ground);


	//Material Test
	auto myMaterial = new UberMaterial();

	//Light
	myMaterial->SetLightDirection(XMFLOAT3(0.577f, 0.577f, 0.577f));
	//Diffuse
	myMaterial->SetDiffuseTexture(L"./Resources/Textures/CobbleStone_DiffuseMap.dds");
	myMaterial->EnableDiffuseTexture(true);
	myMaterial->SetDiffuseColor(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	//Specular
	myMaterial->SetSpecularColor(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	myMaterial->EnableSpecularLevelTexture(false);
	myMaterial->SetShininess(1);
	//Ambient
	myMaterial->SetAmbientColor(XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	myMaterial->SetAmbientIntensity(0.0f);
	//Normal Mapping
	myMaterial->FlipNormalGreenCHannel(false);
	myMaterial->EnableNormalMapping(false);
	//Environment
	myMaterial->SetEnvironmentCube(L"./Resources/Textures/Sunol_Cubemap.dds");
	myMaterial->EnableEnvironmentMapping(true);
	myMaterial->SetReflectionStrength(0.5f);
	myMaterial->SetRefractionStrength(0.0f);
	myMaterial->SetRefractionIndex(0.0f);
	//Opacity
	myMaterial->SetOpacity(1.0f);
	myMaterial->EnableOpacityMap(false);
	//Specular
	myMaterial->EnableSpecularPhong(true);
	//Fresnell
	myMaterial->EnableFresnelFaloff(false);

	gameContext.pMaterialManager->AddMaterial(myMaterial, 60);
	
	auto model = new ModelComponent(L"./Resources/Meshes/Teapot.ovm");
	model->SetMaterial(60);

	auto teapot = new GameObject();
	teapot->AddComponent(model);
	AddChild(teapot);
	teapot->GetTransform()->Translate(0, 0, 20);
}

void UberScene::Update(const GameContext& gameContext)
{

	m_FpsInterval += gameContext.pGameTime->GetElapsed();
	if (m_FpsInterval >= FPS_COUNTER)
	{
		m_FpsInterval -= FPS_COUNTER;
		Logger::LogFormat(LogLevel::Info, L"FPS: %i", gameContext.pGameTime->GetFPS());
	}
}

void UberScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
