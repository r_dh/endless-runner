//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "Ball.h"
#include "Components\Components.h"
#include "Physx/PhysxManager.h"

Ball::Ball(float size, XMFLOAT4 color)
	:
	m_Color(color),
	m_Size(size)
{
}


Ball::~Ball(void)
{
}

void Ball::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	
	// bouncy
	auto defaultMaterial = physX->createMaterial(0, 0, 1);


	auto rigidbody = new RigidBodyComponent();
	rigidbody->SetConstraint(RigidBodyConstraintFlag::TransY, true);
	AddComponent(rigidbody);

	std::shared_ptr<PxGeometry> geom(new PxSphereGeometry(m_Size));
	AddComponent(new ColliderComponent(geom,*defaultMaterial));
}
