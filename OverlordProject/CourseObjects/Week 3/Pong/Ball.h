#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class Ball : public GameObject
{
public:
	Ball(float size, XMFLOAT4 color = (XMFLOAT4)Colors::Red);
	~Ball(void);

	virtual void Initialize(const GameContext& gameContext);

private:

	float m_Size;
	XMFLOAT4 m_Color;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Ball(const Ball& t);
	Ball& operator=(const Ball& t);
};

