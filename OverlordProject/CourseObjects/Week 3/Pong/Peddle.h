#pragma once
#include "Scenegraph/GameObject.h"

class ColliderComponent;

class Peddle : public GameObject
{
public:
	Peddle(float width, float height, float depth, XMFLOAT4 color = (XMFLOAT4)Colors::Red);
	~Peddle(void);

	virtual void Initialize(const GameContext& gameContext);

private:

	XMFLOAT3 m_Dimensions;
	XMFLOAT4 m_Color;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Peddle(const Peddle& t);
	Peddle& operator=(const Peddle& t);
};

