//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "SpriteFontTestScene.h"

#include "Content\ContentManager.h"
#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameObject.h"
#include "Graphics\TextRenderer.h"

SpriteFontTestScene::SpriteFontTestScene(void) :
	GameScene(L"SpriteFontTestScene")
{
}


SpriteFontTestScene::~SpriteFontTestScene(void)
{
}

void SpriteFontTestScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_Font = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Consolas_32.fnt");
	
}

void SpriteFontTestScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void SpriteFontTestScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext); 	
	auto textRdr = TextRenderer::GetInstance();
	textRdr->DrawText(m_Font, L"HELLO WORLD", XMFLOAT2(10.f, 10.0f));
	
}