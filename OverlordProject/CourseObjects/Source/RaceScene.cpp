//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "RaceScene.h"

#include "Scenegraph\GameObject.h"
#include "Diagnostics\Logger.h"
#include "Diagnostics\DebugRenderer.h"
#include "Scenegraph\SceneManager.h"

#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
//#include "Prefabs\Prefabs.h"
#include "Graphics\ModelAnimator.h"
#include "Scenegraph\GameObject.h"
#include "RaceCharacter.h"

#include "Graphics\TextRenderer.h"
#include "Graphics\SpriteRenderer.h"
#include "../../Materials/DiffuseMaterial.h"
#include "../../Materials/UberMaterial.h"
#include "../../Materials/BackGroundMaterial.h"
#include "..\..\Materials\SkinnedDiffuseMaterial.h"
#include "../../../OverlordEngine/Prefabs/SkyBoxPrefab.h"
#include "../../../OverlordEngine/Components/ParticleEmitterComponent.h"
#include "../../../OverlordEngine/Components/SpriteComponent.h"
#include "../../Materials/PostgrayScale.h"
#include "../../Materials/PostRadialBlur.h"
#include "../../Materials/PostBlur.h"
#include "../../Materials/PostFog.h"
#include "../../Materials/PostDepth.h"
#include "Ammo.h"
#include <memory>
#include <stdlib.h>     // srand, rand 
#include <time.h> //time

RaceScene::RaceScene(void) :
GameScene(L"RaceScene") {
}


RaceScene::~RaceScene(void) {
	//--- No memory leaks ---
	delete m_pModel;
	delete m_pAnimator;
	//Prevent memory leaks from ragequitting
	if (m_Blur != nullptr)  RemovePostProcessingEffect(m_Blur);
}

void RaceScene::Initialize(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);
	//		GetPhysxProxy()->EnablePhysxDebugRendering(true);

	//--- Enviroment ---
#pragma region Enviroment
	// Create PhysX ground plane
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	auto bouncyMaterial = physX->createMaterial(0, 0, 1);
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddChild(ground);

	//Create visible ground plane
	DiffuseMaterial *mat_wood = new DiffuseMaterial();
	mat_wood->SetDiffuseTexture(L"Resources/Textures/Chair_Dark.dds");
	gameContext.pMaterialManager->AddMaterial(mat_wood, 1);

	auto groundPlaneMesh = new ModelComponent(L"./Resources/Meshes/regularPlane.ovm");
	groundPlaneMesh->SetMaterial(1);

	m_pGroundPlane = new GameObject();
	m_pGroundPlane->AddComponent(groundPlaneMesh);
	AddChild(m_pGroundPlane);
	m_pGroundPlane->GetTransform()->Rotate(-90, 0, 0);
	m_pGroundPlane->GetTransform()->Scale(50, 50, 50);
	m_pGroundPlane->GetTransform()->Translate(0, -20.f, 0);

	//Background UBER material
	UberMaterial *mat_background = new UberMaterial();

	//Diffuse
	mat_background->SetDiffuseTexture(L"./Resources/Textures/background.jpg");
	//	mat_background->EnableDiffuseTexture(false);
	mat_background->EnableDiffuseTexture(true);
	mat_background->SetDiffuseColor(XMFLOAT4(0.0f, 0.0f, 0.f, 1.0f));
	//Specular
	mat_background->SetSpecularColor(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f));
	mat_background->EnableSpecularLevelTexture(false);
	mat_background->SetShininess(0);
	////Ambient
	mat_background->SetAmbientColor(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	mat_background->SetAmbientIntensity(1.0f);

	//Environment
	mat_background->SetEnvironmentCube(L"./Resources/Textures/NightSky.dds");
	mat_background->EnableEnvironmentMapping(true);
	mat_background->SetReflectionStrength(1.0f);

	//Opacity
	mat_background->SetOpacity(1.0f);
	mat_background->EnableOpacityMap(true);
	mat_background->SetOpacityTexture(L"./Resources/Textures/background.jpg");

	gameContext.pMaterialManager->AddMaterial(mat_background, 5);

	auto backgroundMesh = new ModelComponent(L"./Resources/Meshes/regularPlane.ovm");
	backgroundMesh->SetMaterial(5);

	m_pBackGround = new GameObject();
	m_pBackGround->AddComponent(backgroundMesh);
	m_pBackGround->GetTransform()->Rotate(180, 0, 0);
	m_pBackGround->GetTransform()->Translate(0, 0, 1000);
	m_pBackGround->GetTransform()->Scale(20, 5, 1);
	AddChild(m_pBackGround);


	//Add skybox
	auto skyBox = new SkyBoxPrefab(L"Resources/Textures/NightSky.dds"); //SkyBox_Clouds //SkyBox_Default //SkyBox_Nebula //SkyBox
	AddChild(skyBox);

#pragma endregion Enviroment: Groundplane, skybox, background plane

	//--- HUD ---
#pragma region HUD
	//Ammo HUD element
	auto ammoSprite = new GameObject();
	ammoSprite->AddComponent(new SpriteComponent(L"./Resources/Textures/ammoHUD.png", XMFLOAT2(-0.5f, -1.75f), XMFLOAT4(1, 1, 1, 0.5f)));
	ammoSprite->GetTransform()->Scale(0.5f, 0.5f, 0.5f);
	AddChild(ammoSprite);

	//Killcount HUD element
	auto skullSprite = new GameObject();
	skullSprite->AddComponent(new SpriteComponent(L"./Resources/Textures/killcountHUD.png", XMFLOAT2(-0.2f, -2.f), XMFLOAT4(1, 1, 1, 0.5f)));
	skullSprite->GetTransform()->Scale(0.45f, 0.45f, 0.45f);
	AddChild(skullSprite);

	//Hand overlay HUD
	auto povSprite = new GameObject();
	povSprite->AddComponent(new SpriteComponent(L"./Resources/Textures/gunOverlay.png", XMFLOAT2(-3.6f, -1.75f), XMFLOAT4(1, 1, 1, 1.f)));
	povSprite->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	AddChild(povSprite);

	//Respawn overlay HUD
	//m_pRespawningSprite = new GameObject();
	//m_pRespawningSprite->AddComponent(new SpriteComponent(L"./Resources/Textures/respawn.png", XMFLOAT2(-0.35f, -3.0f), XMFLOAT4(1, 1, 1, 0.f)));
	//m_pRespawningSprite->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	//AddChild(m_pRespawningSprite);

	//Add text
	m_Font = ContentManager::Load<SpriteFont>(L"./Resources/SpriteFonts/Consolas_32.fnt");
#pragma endregion HUD Sprites: Gun, Skull, Ammo

	//--- Particles ---
#pragma region Particles
	m_pRootObject = new GameObject();
	AddChild(m_pRootObject);
	// Smoke
	auto particleObj = new GameObject();
	auto particleEmitter = new ParticleEmitterComponent(L"./Resources/Textures/Smoke.png", 60);
	particleEmitter->SetVelocity(XMFLOAT3(0, 6.0f, 0));
	particleEmitter->SetMinSize(1.0f);
	particleEmitter->SetMaxSize(2.0f);
	particleEmitter->SetMinEnergy(1.0f);
	particleEmitter->SetMaxEnergy(3.0f);
	particleEmitter->SetMinSizeGrow(3.5f);
	particleEmitter->SetMaxSizeGrow(7.5f);
	particleEmitter->SetMinEmitterRange(0.2f);
	particleEmitter->SetMaxEmitterRange(1.9f);
	particleEmitter->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	particleObj->AddComponent(particleEmitter);
	particleObj->GetTransform()->Translate(0, -5, 0);
	m_pRootObject->AddChild(particleObj);
	m_pRootObject->GetTransform()->Translate(-4, 0, 10);
	// Flare
	auto flareParticleObj = new GameObject();
	auto particleEmitterFlare = new ParticleEmitterComponent(L"./Resources/Textures/flare.png", 10);
	particleEmitterFlare->SetVelocity(XMFLOAT3(0, 0, 1));
	particleEmitterFlare->SetMinSize(1.0f);
	particleEmitterFlare->SetMaxSize(3.5f);
	particleEmitterFlare->SetMinEnergy(1.0f);
	particleEmitterFlare->SetMaxEnergy(3.4f);
	particleEmitterFlare->SetMinSizeGrow(3.5f);
	particleEmitterFlare->SetMaxSizeGrow(5.5f);
	particleEmitterFlare->SetMinEmitterRange(0.2f);
	particleEmitterFlare->SetMaxEmitterRange(0.4f);
	particleEmitterFlare->SetColor(XMFLOAT4(1.f, 1.f, 1.f, 0.6f));
	flareParticleObj->AddComponent(particleEmitterFlare);
	flareParticleObj->GetTransform()->Translate(0, -5, 0);
	m_pRootObject->AddChild(flareParticleObj);
#pragma endregion Particles: Smoke and Flare

	//--- Pickups ---
#pragma region Pickups
	//Add ammo
	auto ammoMaterial = new DiffuseMaterial();
	ammoMaterial->SetDiffuseTexture(L"./Resources/Textures/Ammo.jpg");
	gameContext.pMaterialManager->AddMaterial(ammoMaterial, 2);

	m_Ammo = new Ammo();
	m_Ammo->GetTransform()->Rotate(90, 90, 0);
	m_Ammo->GetTransform()->Translate(0, -5, 10);
	AddChild(m_Ammo);
#pragma endregion Pickup: Ammo

	//--- Character Controller ---
#pragma region Character Controller
	// Create Character
	m_pCharacter = new RaceCharacter(2.f, 5.f, 1.f);
	AddChild(m_pCharacter);
#pragma endregion Character Controller

	//--- Skinned Animations ---
#pragma region Skinned Animations
	// ADD ZOMBIE
	auto skinnedDiffuseMaterial = new SkinnedDiffuseMaterial();
	skinnedDiffuseMaterial->SetDiffuseTexture(L"./Resources/Textures/zombie_ao.jpg"); //Knight
	gameContext.pMaterialManager->AddMaterial(skinnedDiffuseMaterial, 0);

	//Make model of animation
	m_pModel = new ModelComponent(L"./Resources/Meshes/Zombie.ovm");  //zombieMesh //zombie //Knight
	m_pModel->SetMaterial(0);
#pragma endregion Skinned Animations

	//--- Postprocessing Shader ---
#pragma region Postprocessing Shader
	//1. Add grey post process
	auto m_Grey = new PostGrayScale();
	m_Grey->Initialize(gameContext);
	AddPostProcessingEffect(m_Grey);

	//2. Add depth darken post process
	auto depth = new PostDepth();
	depth->Initialize(gameContext);
	AddPostProcessingEffect(depth);

	//3. Blur post process, Managed on DEATH!
	/*m_Blur = new PostBlur();
	m_Blur->Initialize(gameContext);
	AddPostProcessingEffect(m_Blur);*/
#pragma endregion First two Postprocessing Effects

	//srand(time(NULL)); //set seed
#pragma region Zombies
	auto standardMaterial = physX->createMaterial(0, 0, 0.1f);
	//ADD CUBES / enemies
	m_EnemyCount = 25; //Adjust parameter (25 - 200) lower is more responsive, less predictable //25
	m_EnemyInterval = 15; //Adjust parameter (5 - 25) lower is harder //Distance between enemies //15
	for (int i = 0; i < m_EnemyCount; i++) {
		//Modelcomponent
		auto obj = new ModelComponent(L"./Resources/Meshes/Zombie.ovm"); //zombieMesh //zombieOld //Zombie
		obj->SetMaterial(0);
		//Main gameobject
		auto gObj = new GameObject();
		gObj->AddComponent(obj);
		//Push back in array
		m_ObstaclesArr.push_back(gObj);
		m_ObstaclesArr.at(i)->GetTransform()->Scale(0.08f, 0.08f, 0.08f);
		m_ObstaclesArr.at(i)->GetTransform()->Translate((float)((rand() % m_SpawnRange) - (m_SpawnRange / 2)), -7.f, (float)(125 + (i * 10))); // initial interval is 10
		AddChild(m_ObstaclesArr.at(i));

		auto cubePrefabZombie = new CubePrefab();

		auto rigidBody = new RigidBodyComponent(false);
		rigidBody->SetKinematic(true);
		cubePrefabZombie->AddComponent(rigidBody);

		std::shared_ptr<PxGeometry> boxGeom(new PxBoxGeometry(3.5, 7, 2));
		cubePrefabZombie->AddComponent(new ColliderComponent(boxGeom, *standardMaterial));
		auto zombiePos = m_ObstaclesArr.at(i)->GetTransform()->GetPosition();
		cubePrefabZombie->GetTransform()->Translate(zombiePos.x, zombiePos.y + 7, zombiePos.z);
		m_CollisionArr.push_back(cubePrefabZombie);
		AddChild(m_CollisionArr.at(i));
	}
#pragma endregion Zombies

	//--- Cooked mesh collision ---
#pragma region Cooked mesh collision
	//Trees
	auto treeBarkMaterial = new DiffuseMaterial();
	treeBarkMaterial->SetDiffuseTexture(L"./Resources/Textures/TreeBark.jpg");
	gameContext.pMaterialManager->AddMaterial(treeBarkMaterial, 6);

	for (int i = 0; i < m_TreeCount; i++) {
		//Model
		ModelComponent* treeModel = new ModelComponent(L"./Resources/Meshes/Trees.ovm");
		treeModel->SetMaterial(6);
		//Rigidbody
		auto treeRigidBody = new RigidBodyComponent(false);
		treeRigidBody->SetKinematic(true);
		//Triangle mesh collision
		auto treeMesh = ContentManager::Load<PxTriangleMesh>(L"./Resources/Meshes/Trees.ovpt");
		shared_ptr<PxGeometry> treeGeometry(new PxTriangleMeshGeometry(treeMesh));
		auto treeCollider = new ColliderComponent(treeGeometry, *standardMaterial);

		GameObject* tree = new GameObject();
		tree->AddComponent(treeModel);
		tree->AddComponent(treeRigidBody);
		tree->AddComponent(treeCollider);
		m_TreesArr.push_back(tree);
		AddChild(m_TreesArr.at(i));
		m_TreesArr.at(i)->GetTransform()->Translate(float((rand() % m_SpawnRange) - (m_SpawnRange / 2)), 0.f, 482.5f + 300 * i);
	}
#pragma endregion Trees trianglecollision

}

void RaceScene::Update(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);
	//---Zombie positions---
#pragma region Zombies
	auto actorPos = m_pCharacter->GetTransform()->GetPosition();
	//Update groundplane position
	auto groundPos = m_pGroundPlane->GetTransform()->GetPosition();
	m_pGroundPlane->GetTransform()->Translate(actorPos.x, groundPos.y, actorPos.z);
	//If actor walks past first enemy
	if (actorPos.z > m_ObstaclesArr.at(0)->GetTransform()->GetPosition().z) {
		//Give the enemy a new random x position and set z pos behind last enemy
		m_ObstaclesArr.at(0)->GetTransform()->Translate(
			actorPos.x + ((rand() % m_SpawnRange) - (m_SpawnRange / 2)),
			-7,
			m_ObstaclesArr.at(m_ObstaclesArr.size() - 1)->GetTransform()->GetPosition().z + m_EnemyInterval);

		//Move it's rigidbody along
		auto newSpot = m_ObstaclesArr.at(0)->GetTransform()->GetPosition();
		m_CollisionArr.at(0)->GetTransform()->Translate(newSpot.x, newSpot.y + 7, newSpot.z);

		//Restart animation
		m_ObstaclesArr.at(0)->GetComponent<ModelComponent>()->GetAnimator()->Reset();
	}

	//Sort all enemies on z position
	std::sort(m_ObstaclesArr.begin(), m_ObstaclesArr.end(), [](GameObject* a, GameObject* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });
	std::sort(m_CollisionArr.begin(), m_CollisionArr.end(), [](CubePrefab* a, CubePrefab* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });

	//Adjust collision positions
	for (size_t i = 0; i < m_ObstaclesArr.size(); i++) {
		auto newSpot = m_ObstaclesArr.at(i)->GetTransform()->GetPosition();
		m_CollisionArr.at(i)->GetTransform()->Translate(newSpot.x, newSpot.y + 7, newSpot.z);
	}
#pragma endregion Zombies and colliders

	//--- Raycasting ---
#pragma region Raycasting
	//Kill zombie
	if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON) && !gameContext.pInput->IsMouseButtonDown(VK_LBUTTON, true) && m_AmmoCount > 0) {
		m_AmmoCount -= 1;

		//Sort both arrays to prevent incompatibility
		std::sort(m_ObstaclesArr.begin(), m_ObstaclesArr.end(), [](GameObject* a, GameObject* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });
		std::sort(m_CollisionArr.begin(), m_CollisionArr.end(), [](CubePrefab* a, CubePrefab* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });

		GameObject* zombie = dynamic_cast<CubePrefab*>(gameContext.pCamera->Pick(gameContext));

		if (zombie != nullptr) {
			auto index = std::find_if(m_CollisionArr.begin(), m_CollisionArr.end(), [zombie](GameObject* a) -> bool { return a == zombie; });
			int zombieIndex = index - m_CollisionArr.begin();
			if (index != m_CollisionArr.end()) m_ObstaclesArr.at(zombieIndex)->GetTransform()->Translate(
				actorPos.x + ((rand() % m_SpawnRange) - (m_SpawnRange / 2)),
				-7,
				m_ObstaclesArr.at(m_ObstaclesArr.size() - 1)->GetTransform()->GetPosition().z + m_EnemyInterval);
			else Logger::LogInfo(L"Zombie not found");

			++m_KillCounter;
			//Sort both arrays to prevent incompatibility
			std::sort(m_ObstaclesArr.begin(), m_ObstaclesArr.end(), [](GameObject* a, GameObject* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });

			//Adjust collision positions
			for (size_t i = 0; i < m_ObstaclesArr.size(); i++) {
				auto newSpot = m_ObstaclesArr.at(i)->GetTransform()->GetPosition();
				m_CollisionArr.at(i)->GetTransform()->Translate(newSpot.x, newSpot.y + 7, newSpot.z);
			}

			std::sort(m_CollisionArr.begin(), m_CollisionArr.end(), [](CubePrefab* a, CubePrefab* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });

		} else Logger::LogInfo(L"Zombie missed");
	}
#pragma endregion Kill zombies

	//--- Pickups ---
#pragma region Pickups
	//Ammobox position and pick up
	m_Ammo->GetTransform()->Rotate(0, 60, 0);
	if (actorPos.z > m_Ammo->GetTransform()->GetPosition().z) {
		float distance = abs(actorPos.x - m_Ammo->GetTransform()->GetPosition().x);
		if (distance < 10) m_AmmoCount = 10;
		m_Ammo->GetTransform()->Translate(actorPos.x + ((rand() % m_SpawnRange) - (m_SpawnRange / 2)), -7, actorPos.z + 3000);
		auto ammoPos = m_Ammo->GetTransform()->GetPosition();
		m_pRootObject->GetTransform()->Translate(ammoPos.x - 5, -2, ammoPos.z - 6);
	}
#pragma endregion Ammo

	//--- Cooked mesh collision ---
#pragma region Cooked mesh collision
	//Trees
	if (actorPos.z > m_TreesArr.at(0)->GetTransform()->GetPosition().z) {
		m_TreesArr.at(0)->GetTransform()->Translate(
			actorPos.x + ((rand() % m_SpawnRange) - (m_SpawnRange / 2)),
			0,
			m_TreesArr.at(m_TreesArr.size() - 1)->GetTransform()->GetPosition().z + 350);

		std::sort(m_TreesArr.begin(), m_TreesArr.end(), [](GameObject* a, GameObject* b) -> bool { return a->GetTransform()->GetPosition().z < b->GetTransform()->GetPosition().z; });
	}
#pragma endregion Update tree positions

	//Background plane
	m_pBackGround->GetTransform()->Translate(0, 0, actorPos.z + 1000);

	//--- Death and 3th post processing effect ---
#pragma region Death
	if (!m_IsDead) {
		if ((int)actorPos.z > (int)m_Distance) m_Distance = (int)actorPos.z;
		else if (actorPos.z > 20) {
			if (m_DeadTimer < 3) m_DeadTimer += 0.5f; //Prevent insta death
			else {
				if (!m_Blur) {
					m_Blur = new PostBlur();
					m_Blur->Initialize(gameContext);
					AddPostProcessingEffect(m_Blur);
				
					//If possible, add 'Respawning' overlay here
				/*	m_pRespawningSprite = new GameObject();
					//m_pRespawningSprite->AddComponent(new SpriteComponent(L"./Resources/Textures/respawn.png", XMFLOAT2(-0.f, -0.0f), XMFLOAT4(1, 1, 1, 1.f)));
					m_pRespawningSprite->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
					m_pRespawningSprite->GetComponent<SpriteComponent>()->SetPivot(XMFLOAT2(-100.5f, -100.5f)); //XMFLOAT2(-0.4f, -3.5f)
					AddChild(m_pRespawningSprite);*/

				}
				m_IsDead = true;
				Logger::LogInfo(L"You died!");
				Logger::LogInfo(std::to_wstring(actorPos.z));
				Logger::LogInfo(std::to_wstring(m_Distance));
				Logger::LogInfo(L"Respawning in: 3");

			}
		}
	}

	if (m_IsDead) {
		m_DeadTimer -= gameContext.pGameTime->GetElapsed();
		if (m_DeadTimer <= 0.01f) {
			//m_DeadTimer = 3.f;
			m_IsDead = false;
			m_Distance = 0;
			m_pCharacter->GetTransform()->Translate(actorPos.x, actorPos.y, actorPos.z - 300);
			if (m_Blur) RemovePostProcessingEffect(m_Blur);
			//RemoveChild(m_pRespawningSprite);
			m_Blur = nullptr;
			SceneManager::GetInstance()->NextScene();
		}

		if (m_DeadTimer - (int)m_DeadTimer < gameContext.pGameTime->GetElapsed()) Logger::LogInfo(L"Respawning in: " + std::to_wstring((int)m_DeadTimer));
	}
#pragma endregion Death and third post processing effect
}

void RaceScene::Draw(const GameContext& gameContext) {
	// --- Zombie animations ---
#pragma region Animations
	UNREFERENCED_PARAMETER(gameContext);
	if (m_pAnimator == nullptr) {
		m_pAnimator = m_pModel->GetAnimator();

		for (auto actor : m_ObstaclesArr) {
			actor->GetComponent<ModelComponent>()->GetAnimator()->Play();
			actor->GetComponent<ModelComponent>()->GetAnimator()->SetAnimationSpeed(1.4f - ((rand() % 60) / 100));
		}
	}
#pragma endregion Update zombie animations

	//--- HUD ---
#pragma region HUD
	//Draw distance on screen
	TextRenderer::GetInstance()->DrawText(m_Font, std::to_wstring((int)m_pCharacter->GetTransform()->GetPosition().z) + L"m", XMFLOAT2(10.f, 10.0f));

	std::wstring ammo = std::to_wstring(m_AmmoCount);
	if (ammo.length() == 1) ammo = L"0" + ammo;
	TextRenderer::GetInstance()->DrawText(m_Font, ammo, XMFLOAT2(50.f, 40.0f));
	TextRenderer::GetInstance()->DrawText(m_Font, std::to_wstring(m_KillCounter), XMFLOAT2(50.f, 75.0f));
	//if (m_IsDead) {
	//	auto tex = ContentManager::Load<TextureData>(L"./Resources/Textures/respawn.png");
	//	SpriteRenderer::GetInstance()->Draw(tex, XMFLOAT2(500, 200), XMFLOAT4(1, 1, 1, 0.5f), XMFLOAT2(0, 0), XMFLOAT2(5, 5));
	//}
	//m_pRespawningSprite;
#pragma endregion HUD, distance, ammo, kills
}
