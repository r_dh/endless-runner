#pragma once
#include "Scenegraph/GameScene.h"
#include "Helpers\EffectHelper.h"
#include "../../Materials/CubeTerrainMaterial.h"

class Material;
class CubeTerrainMaterial;

class TerrainScene: public GameScene
{
public:
	TerrainScene(void);
	virtual ~TerrainScene(void);

	enum InputActions : UINT
	{
		MAPNEXT = 0,
		MAPPREV,
		PLANENEXT,
		PLANEPREV,
		ZOOMIN,
		ZOOMOUT
	};

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:

	float m_FpsInterval;
	static const int HEIGHT_MAPS = 5, PLANE_MESHES = 3;
	CubeTerrainMaterial *m_CubeTerrainMaterial[HEIGHT_MAPS];
	GameObject *m_Terrain = nullptr;
	ModelComponent *m_GroundPlane[PLANE_MESHES];
	float m_Zoomlevel = 4;
	int	m_PlaneIndex = 0,
		m_MaterialIndex = 0;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	TerrainScene( const TerrainScene &obj);
	TerrainScene& operator=( const TerrainScene& obj);
};

