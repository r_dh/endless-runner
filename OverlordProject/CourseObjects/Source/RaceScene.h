//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
#pragma once
#include "Scenegraph/GameScene.h"
#include "Helpers\EffectHelper.h"
#include "Prefabs\Prefabs.h"

class RaceCharacter;
class ModelComponent;
class GameObject;
class ModelAnimator;
class SpriteFont;
class Ammo;
class PostBlur;

class RaceScene : public GameScene
{
public:
	RaceScene(void);
	virtual ~RaceScene(void);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:
	RaceCharacter *m_pCharacter = nullptr;
	ModelComponent *m_pModel = nullptr;
	GameObject *m_pEnemy = nullptr;
	GameObject *m_pGroundPlane = nullptr, *m_pBackGround = nullptr;
	GameObject  *m_pRootObject = nullptr; //*m_pRespawningSprite = nullptr,
	ModelAnimator *m_pAnimator = nullptr;
	SpriteFont *m_Font = nullptr;
	PostBlur *m_Blur = nullptr;
	std::vector<GameObject*> m_ObstaclesArr;
	std::vector<GameObject*> m_TreesArr;
	std::vector<CubePrefab*> m_CollisionArr;
	int m_EnemyCount = 25, m_EnemyInterval = 25, m_SpawnRange = 200, //500
		m_AmmoCount = 10, m_TreeCount = 5, m_KillCounter = 0, m_Distance = 0;
	float m_DeadTimer = 3.f;
	bool m_IsDead = false;
	Ammo *m_Ammo = nullptr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	RaceScene( const RaceScene &obj);
	RaceScene& operator=( const RaceScene& obj);
};


