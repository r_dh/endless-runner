#include "Base\stdafx.h"
#include "Ammo.h"

#include "Components\Components.h"
#include "Physx/PhysxManager.h"
#include "Content/ContentManager.h"
#include "Components/ModelComponent.h"
#include "../../../OverlordEngine/Components/ParticleEmitterComponent.h"

Ammo::Ammo() 
{
}

Ammo::~Ammo(void) {
}

void Ammo::Initialize(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	ModelComponent* model = new ModelComponent(L"./Resources/Meshes/Ammo.ovm");
	model->SetMaterial(2);

	AddComponent(model);

}

void Ammo::Update(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);
}
