#pragma once
#include "Scenegraph/GameObject.h"

class Ammo : public GameObject
{
public:
	Ammo();
	virtual ~Ammo();

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);


private:

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Ammo(const Ammo& t);
	Ammo& operator=(const Ammo& t);
};
