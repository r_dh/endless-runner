//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "TerrainScene.h"
#include "Scenegraph\GameObject.h"
#include "Diagnostics\Logger.h"
#include "Diagnostics\DebugRenderer.h"

#include "Prefabs\Prefabs.h"
#include "Components\Components.h"
#include "Physx\PhysxProxy.h"
#include "Physx\PhysxManager.h"
#include "Content\ContentManager.h"
#include "Graphics\MeshFilter.h"

#include "Components\ModelComponent.h"

#define FPS_COUNTER 1

TerrainScene::TerrainScene(void) :
GameScene(L"TerrainScene"),
m_FpsInterval(FPS_COUNTER)

{
	//TODO: maak m_CubeTerrainMaterial en m_GroundPlane index en gebruik in update
	for (int i = 0; i < HEIGHT_MAPS; i++) {
		m_CubeTerrainMaterial[i] = nullptr;
	}
	for (int i = 0; i < PLANE_MESHES; i++) {
		m_GroundPlane[i] = nullptr;
	}
}


TerrainScene::~TerrainScene(void) {
	
		delete m_GroundPlane[0];
		delete m_GroundPlane[1];
}

void TerrainScene::Initialize(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);

	//GetPhysxProxy()->EnablePhysxDebugRendering(true);
	gameContext.pGameTime->ForceElapsedUpperbound(true);

	// Create PhysX ground plane
	auto physX = PhysxManager::GetInstance()->GetPhysics();
	GetPhysxProxy()->EnablePhysxDebugRendering(true);

	//auto ground = new GameObject();
	m_Terrain = new GameObject();


	m_CubeTerrainMaterial[0] = new CubeTerrainMaterial();

	m_CubeTerrainMaterial[0]->SetDiffuseTexture(L"./Resources/Textures/TestSprite.jpg");
	m_CubeTerrainMaterial[0]->SetNoiseTexture(L"./Resources/Textures/noisemap.jpg"); //reunion //heigthMap //noisemap //mallorca //myst
	m_CubeTerrainMaterial[0]->SetZoomLevel(16);
	gameContext.pMaterialManager->AddMaterial(m_CubeTerrainMaterial[0], 0);

	m_CubeTerrainMaterial[1] = new CubeTerrainMaterial();
	m_CubeTerrainMaterial[1]->SetNoiseTexture(L"./Resources/Textures/heigthMap.jpg"); //reunion //heigthMap //noisemap //mallorca //myst
	m_CubeTerrainMaterial[1]->SetZoomLevel(16);
	gameContext.pMaterialManager->AddMaterial(m_CubeTerrainMaterial[1], 1);

	m_CubeTerrainMaterial[2] = new CubeTerrainMaterial();
	m_CubeTerrainMaterial[2]->SetNoiseTexture(L"./Resources/Textures/reunion.jpg"); //reunion //heigthMap //noisemap //mallorca //myst
	m_CubeTerrainMaterial[2]->SetZoomLevel(16);
	gameContext.pMaterialManager->AddMaterial(m_CubeTerrainMaterial[2], 2);

	m_CubeTerrainMaterial[3] = new CubeTerrainMaterial();
	m_CubeTerrainMaterial[3]->SetNoiseTexture(L"./Resources/Textures/mallorca.jpg"); //reunion //heigthMap //noisemap //mallorca //myst
	m_CubeTerrainMaterial[3]->SetZoomLevel(16);
	gameContext.pMaterialManager->AddMaterial(m_CubeTerrainMaterial[3], 3);

	m_CubeTerrainMaterial[4] = new CubeTerrainMaterial();
	m_CubeTerrainMaterial[4]->SetNoiseTexture(L"./Resources/Textures/myst.jpg"); //reunion //heigthMap //noisemap //mallorca //myst
	m_CubeTerrainMaterial[4]->SetZoomLevel(16);
	gameContext.pMaterialManager->AddMaterial(m_CubeTerrainMaterial[4], 4);

	m_GroundPlane[0] = new ModelComponent(L"./Resources/Meshes/regularPlane.ovm");
	m_GroundPlane[1] = new ModelComponent(L"./Resources/Meshes/GroundPlane.ovm");
	m_GroundPlane[2] = new ModelComponent(L"./Resources/Meshes/precisePlane.ovm");

	for (int i = 0; i < PLANE_MESHES; i++) {
		m_GroundPlane[i]->SetMaterial(0);
		//m_GroundPlane[i]->GetTransform()->Translate(0, 0, 500 * i);
		//m_Terrain->AddComponent(m_GroundPlane[i]);	
	}

	m_Terrain->AddComponent(m_GroundPlane[2]); //0 or 1 for less precise plane
	AddChild(m_Terrain);
	m_Terrain->GetTransform()->Rotate(90.f, 0.f, 0.f);

	InputAction nextMap = InputAction(MAPNEXT, Pressed, VK_RIGHT);
	InputAction prevMap = InputAction(MAPPREV, Pressed, VK_LEFT);
	//InputAction nextPlane = InputAction(PLANENEXT, Pressed, VK_UP);
	//InputAction prevPlane = InputAction(PLANEPREV, Pressed, VK_DOWN);
	InputAction zoomIn = InputAction(ZOOMIN, Pressed, VK_ADD);
	InputAction zoomOut = InputAction(ZOOMOUT, Pressed, VK_SUBTRACT);

	gameContext.pInput->AddInputAction(nextMap);
	gameContext.pInput->AddInputAction(prevMap);
	//gameContext.pInput->AddInputAction(nextPlane);
	//gameContext.pInput->AddInputAction(prevPlane);
	gameContext.pInput->AddInputAction(zoomIn);
	gameContext.pInput->AddInputAction(zoomOut);
}

void TerrainScene::Update(const GameContext& gameContext) {

	m_FpsInterval += gameContext.pGameTime->GetElapsed();
	if (m_FpsInterval >= FPS_COUNTER) {
		m_FpsInterval -= FPS_COUNTER;
		Logger::LogFormat(LogLevel::Info, L"FPS: %i", gameContext.pGameTime->GetFPS());
	}
	//CubeTerrainMaterial *tst = m_Terrain->GetComponent<ModelComponent>()->GetGameObject()->GetComponent<CubeTerrainMaterial>(); //->SetZoomLevel(m_Zoomlevel += m_Zoomlevel);
	if (gameContext.pInput->IsActionTriggered(ZOOMIN)) m_CubeTerrainMaterial[m_MaterialIndex]->SetZoomLevel((m_Zoomlevel += 4)); //m_Zoomlevel
	if (gameContext.pInput->IsActionTriggered(ZOOMOUT) && m_Zoomlevel > 4) m_CubeTerrainMaterial[m_MaterialIndex]->SetZoomLevel((m_Zoomlevel -= 4)); // m_Zoomlevel / 

	//if (gameContext.pInput->IsActionTriggered(PLANENEXT)) {
		//m_Terrain->GetComponent<ModelComponent>()->SetMaterial(++m_PlaneIndex % PLANE_MESHES);
		//m_Terrain->GetComponent<ModelComponent>()->GetTransform()->Translate(0, 0, -500);
		//m_Terrain->RemoveComponent(m_GroundPlane[0]);
		
	//}
	//if (gameContext.pInput->IsActionTriggered(PLANEPREV) && m_PlaneIndex > 0) m_Terrain->GetComponent<ModelComponent>()->SetMaterial(--m_PlaneIndex % PLANE_MESHES);

	if (gameContext.pInput->IsActionTriggered(MAPNEXT) && m_MaterialIndex < (HEIGHT_MAPS -1)) m_Terrain->GetComponent<ModelComponent>()->SetMaterial((m_MaterialIndex += 1) % HEIGHT_MAPS);
	if (gameContext.pInput->IsActionTriggered(MAPPREV) && m_MaterialIndex > 0) m_Terrain->GetComponent<ModelComponent>()->SetMaterial((m_MaterialIndex -= 1) % HEIGHT_MAPS);
	
}

void TerrainScene::Draw(const GameContext& gameContext) {
	UNREFERENCED_PARAMETER(gameContext);
}