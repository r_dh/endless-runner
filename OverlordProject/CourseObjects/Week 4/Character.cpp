//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.3.133
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]

#include "Base\stdafx.h"
#include "Character.h"
#include "Components\Components.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameScene.h"
#include "Physx/PhysxManager.h"
#include "Physx/PhysxProxy.h"
#include "Diagnostics/Logger.h"

Character::Character(float radius, float height, float moveSpeed) :
m_Radius(radius),
m_Height(height),
m_MoveSpeed(moveSpeed),
m_pCamera(nullptr),
m_pController(nullptr),
m_TotalPitch(0),
m_TotalYaw(0),
m_RotationSpeed(90.f),
m_MaxRunVelocity(50.f),  //Running
m_TerminalVelocity(20),
m_Gravity(9.81f),
m_RunAccelerationTime(0.3f),
m_JumpAccelerationTime(0.8f),
m_RunAcceleration(m_MaxRunVelocity / m_RunAccelerationTime),
m_JumpAcceleration(m_Gravity / m_JumpAccelerationTime),
m_RunVelocity(0),
m_JumpVelocity(0),
m_Velocity(0, 0, 0)
{
}


Character::~Character(void)
{
}

void Character::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	auto physX = PhysxManager::GetInstance()->GetPhysics();
	auto characterMaterial = physX->createMaterial(0, 0, 1);

	// Create controller
	m_pController = new ControllerComponent(characterMaterial, m_Radius, m_Height);
	AddComponent(m_pController);

	// Add a fixed camera as child
	FixedCamera* fixedCam = new FixedCamera();
	AddChild(fixedCam);

	// Register all Input Actions
	InputAction forward = InputAction(FORWARD, Down, 'Z');
	InputAction backward = InputAction(BACKWARD, Down, 'S');
	InputAction left = InputAction(LEFT, Down, 'Q');
	InputAction right = InputAction(RIGHT, Down, 'D');
	InputAction jump = InputAction(JUMP, Pressed, VK_SPACE);


	gameContext.pInput->AddInputAction(forward);
	gameContext.pInput->AddInputAction(backward);
	gameContext.pInput->AddInputAction(left);
	gameContext.pInput->AddInputAction(right);
	gameContext.pInput->AddInputAction(jump);
}

void Character::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	// Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	m_pCamera = GetChild< FixedCamera >()->GetComponent< CameraComponent >();
	m_pCamera->SetActive();
}


void Character::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//Update the character (Camera rotation, Character Movement, Character Gravity)
	if (m_pCamera->IsActive())
	{
		XMFLOAT2 direction = XMFLOAT2(0, 0);
		if (gameContext.pInput->IsActionTriggered(FORWARD)) direction.y = 1;
		else if (gameContext.pInput->IsActionTriggered(BACKWARD)) direction.y = -1;

		if (gameContext.pInput->IsActionTriggered(LEFT)) direction.x = -1;
		else if (gameContext.pInput->IsActionTriggered(RIGHT)) direction.x = 1;

		XMFLOAT2 viewDir = XMFLOAT2(0, 0);
		if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON)) {
			viewDir.x = static_cast<float>(gameContext.pInput->GetMouseMovement().x);
			viewDir.y = static_cast<float>(gameContext.pInput->GetMouseMovement().y);
		}

		//CALCULATE TRANSFORMS
		XMVECTOR forward = XMLoadFloat3(&GetTransform()->GetForward());
		XMVECTOR right = XMLoadFloat3(&GetTransform()->GetRight());

		m_TotalYaw += viewDir.x * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
		m_TotalPitch += viewDir.y * m_RotationSpeed * gameContext.pGameTime->GetElapsed();
		GetTransform()->Rotate(m_TotalPitch, m_TotalYaw, 0);

		XMVECTOR moveDir = forward * direction.y + right * direction.x;
		const float verticalVel = m_Velocity.y; 

		m_RunVelocity += m_RunAcceleration * gameContext.pGameTime->GetElapsed();
		if (m_RunVelocity > m_MaxRunVelocity) m_RunVelocity = m_MaxRunVelocity;
		XMStoreFloat3(&m_Velocity, moveDir * m_RunVelocity);
		m_Velocity.y = verticalVel;


		if (m_pController->GetCollisionFlags() != PxControllerCollisionFlag::eCOLLISION_DOWN) {
			m_JumpVelocity -= m_JumpAcceleration * gameContext.pGameTime->GetElapsed();
			if (m_JumpVelocity > m_TerminalVelocity) m_JumpVelocity = m_TerminalVelocity;		
		}
		else if (gameContext.pInput->IsActionTriggered(JUMP) && m_pController->GetCollisionFlags().isSet(PxControllerCollisionFlag::eCOLLISION_DOWN)) {
			m_JumpVelocity = 0;
			m_Velocity.y = 200;
		}
		else m_Velocity.y = 0;
		

		m_Velocity.y += m_JumpVelocity;

		XMFLOAT3 dispTot = m_Velocity;
		dispTot.x *= gameContext.pGameTime->GetElapsed();
		dispTot.y *= gameContext.pGameTime->GetElapsed();
		dispTot.z *= gameContext.pGameTime->GetElapsed();

		m_pController->Move(dispTot);
	}

}


