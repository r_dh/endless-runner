//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.81
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "ModelTestScene.h"

#include "Scenegraph/GameObject.h"
#include "Components/ModelComponent.h"
#include "../../Materials/ColorMaterial.h"
#include "../../Materials/DiffuseMaterial.h"
#include "Components/RigidBodyComponent.h"
#include "Components/ColliderComponent.h"
#include "Content/ContentManager.h"
#include "Physx/PhysxManager.h"
#include "Components/TransformComponent.h"


ModelTestScene::ModelTestScene(void) :
GameScene(L"ModelTestScene"),
m_pChair(nullptr)
{
}


ModelTestScene::~ModelTestScene(void)
{
}

void ModelTestScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//GROUND PLANE
	//************
	auto physX = PhysxManager::GetInstance()->GetPhysics();

	auto bouncyMaterial = physX->createMaterial(0, 0, 1);
	auto ground = new GameObject();
	ground->AddComponent(new RigidBodyComponent(true));

	std::shared_ptr<PxGeometry> geom(new PxPlaneGeometry());
	ground->AddComponent(new ColliderComponent(geom, *bouncyMaterial, PxTransform(PxQuat(XM_PIDIV2, PxVec3(0, 0, 1)))));
	AddChild(ground);

	//CHAIR OBJECT
	//************
	m_pChair = new GameObject();

	//1. Attach a modelcomponent (chair.ovm)

	m_pChair->AddComponent(new ModelComponent(L"Resources/Meshes/Chair.ovm"));
	
	//2. Create a ColorMaterial and add it to the material manager
	ColorMaterial *mat_red = new ColorMaterial;
	
	gameContext.pMaterialManager->AddMaterial(mat_red, 1);
	
	//3. Assign the material to the previous created modelcomponent
	
	m_pChair->GetComponent<ModelComponent>()->SetMaterial(1);
	// Build and Run
	
	//4. Create a DiffuseMaterial (using PosNormTex3D.fx)
	//		Make sure you are able to set a texture (DiffuseMaterial::SetDiffuseTexture(const wstring& assetFile))
	//		Load the correct shadervariable and set it during the material variable update

	DiffuseMaterial *mat_wood = new DiffuseMaterial();
	mat_wood->SetDiffuseTexture(L"Resources/Textures/Chair_Dark.dds");
	mat_wood->SetEffectVariables(gameContext, m_pChair->GetComponent<ModelComponent>());
	gameContext.pMaterialManager->AddMaterial(mat_wood, 2);
	
	//5. Assign the material to the modelcomponent
	
	m_pChair->GetComponent<ModelComponent>()->SetMaterial(2);
	// Build and Run

	//6. Attach a rigidbody component (pure-dynamic)
	
	m_pChair->AddComponent(new RigidBodyComponent(true));
	
	//7. Attach a collider component (Use a PxConvexMeshGeometry [chair.ovpc])
	std::shared_ptr<PxGeometry> chairgeom(new PxConvexMeshGeometry(ContentManager::Load<PxConvexMesh>(L"Resources/Meshes/Chair.ovpc")));
	m_pChair->AddComponent(new ColliderComponent(chairgeom, *bouncyMaterial));
	// Build and Run
	
	AddChild(m_pChair);

}

void ModelTestScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void ModelTestScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
