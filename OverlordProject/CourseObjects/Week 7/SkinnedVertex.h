#pragma once
#include "Base\stdafx.h"
#include "Helpers\VertexHelper.h"

struct SkinnedVertex
{
	SkinnedVertex(XMFLOAT3 position, XMFLOAT3 normal, XMFLOAT4 color, float blendWeight0 = 1.f, float blendWeight1 = 1.f) :
		TransformedVertex(position, normal, color),
		OriginalVertex(position, normal, color),
		m_BlendWeightFirst(blendWeight0),
		m_BlendWeightSecond(blendWeight1)
		{}
	VertexPosNormCol TransformedVertex;
	VertexPosNormCol OriginalVertex;
	float m_BlendWeightFirst, m_BlendWeightSecond;
};