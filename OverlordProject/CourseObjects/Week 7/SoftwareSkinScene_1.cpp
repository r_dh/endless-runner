//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "SoftwareSkinScene_1.h"

#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameObject.h"
#include "..\..\Materials\ColorMaterial.h"

SoftwareSkinScene_1::SoftwareSkinScene_1(void) :
GameScene(L"SoftwareSkinScene_1"),
m_pBone0(nullptr),
m_pBone1(nullptr),
m_BoneRotation(0),
m_RotationSign(1)
{
}


SoftwareSkinScene_1::~SoftwareSkinScene_1(void)
{
}

void SoftwareSkinScene_1::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	gameContext.pMaterialManager->AddMaterial(new ColorMaterial, 0);

	GameObject *root = new GameObject();

	m_pBone0 = new BoneObject(0, 0, 15);
	m_pBone1 = new BoneObject(1, 0, 15);
	m_pBone0->AddBone(m_pBone1);
	root->AddChild(m_pBone0);

	root->GetTransform()->Rotate(0.f, -90.f, 0.f);


	AddChild(root);
	//Empty GO
	//-SimpleBone_0
	//--EmptyGO -> scale
	//---BoneMesh
	//--SimpleBone_1
	//---EmptyGO -> scale
	//---BoneMesh

}

void SoftwareSkinScene_1::Update(const GameContext& gameContext)
{
	m_BoneRotation += gameContext.pGameTime->GetElapsed() * 45 * m_RotationSign;

	if (m_RotationSign < 0 && m_BoneRotation <= -45) m_RotationSign = 1;
	if (m_RotationSign > 0 && m_BoneRotation >= 45) m_RotationSign = -1;

	m_pBone0->GetTransform()->Rotate(m_BoneRotation, 0.f, 0.f);
	m_pBone1->GetTransform()->Rotate(-m_BoneRotation*2, 0.f, 0.f);
}

void SoftwareSkinScene_1::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
