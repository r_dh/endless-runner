//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.108
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "SoftwareSkinScene_2.h"

#include "Components\Components.h"
#include "Physx\PhysxManager.h"
#include "Physx\PhysxProxy.h"
#include "Prefabs\Prefabs.h"
#include "Scenegraph\GameObject.h"
#include "..\..\Materials\ColorMaterial.h"
#include "SkinnedVertex.h"

SoftwareSkinScene_2::SoftwareSkinScene_2(void) :
GameScene(L"SoftwareSkinScene_2"),
m_pBone0(nullptr),
m_pBone1(nullptr),
m_BoneRotation(0),
m_RotationSign(1)
{
}


SoftwareSkinScene_2::~SoftwareSkinScene_2(void)
{
}

void SoftwareSkinScene_2::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	gameContext.pMaterialManager->AddMaterial(new ColorMaterial(true), 0);

	GameObject *root = new GameObject();

	m_pBone0 = new BoneObject(0, 0, 15);
	m_pBone1 = new BoneObject(1, 0, 15);

	m_pBone0->AddBone(m_pBone1);


	root->AddChild(m_pBone0);
	root->GetTransform()->Rotate(0.f, -90.f, 0.f);
	AddChild(root);
	m_pBone0->CalculateBindPose();

	m_pMeshDrawer = new MeshDrawComponent(24);

	GameObject *meshDrawer = new GameObject();
	meshDrawer->AddComponent(m_pMeshDrawer);
	AddChild(meshDrawer);

	CreateMesh();

}

void SoftwareSkinScene_2::Update(const GameContext& gameContext)
{
	m_BoneRotation += gameContext.pGameTime->GetElapsed() * 45 * m_RotationSign;

	if (m_RotationSign < 0 && m_BoneRotation <= -45) m_RotationSign = 1;
	if (m_RotationSign > 0 && m_BoneRotation >= 45) m_RotationSign = -1;

	m_pBone0->GetTransform()->Rotate(m_BoneRotation, 0.f, 0.f);
	m_pBone1->GetTransform()->Rotate(-m_BoneRotation*2, 0.f, 0.f);

	//Transform bones
	auto world0 = XMLoadFloat4x4(&m_pBone0->GetTransform()->GetWorld());
	auto bindpos0 = XMLoadFloat4x4(&m_pBone0->GetBindPose());
	auto transform0 = XMMatrixMultiply(bindpos0, world0);

	auto world1 = XMLoadFloat4x4(&m_pBone1->GetTransform()->GetWorld());
	auto bindpos1 = XMLoadFloat4x4(&m_pBone1->GetBindPose());
	auto transform1 = XMMatrixMultiply(bindpos1, world1);

	for (UINT i = 0; i < m_SkinnedVertices.size(); i++){
		if (i < 24){
			XMVECTOR newPos = XMVector3TransformCoord(XMLoadFloat3(&m_SkinnedVertices.at(i).OriginalVertex.Position), transform0);
			XMStoreFloat3(&m_SkinnedVertices.at(i).TransformedVertex.Position, newPos);
		}
		else {
			XMVECTOR newPos = XMVector3TransformCoord(XMLoadFloat3(&m_SkinnedVertices.at(i).OriginalVertex.Position), transform1);
			XMStoreFloat3(&m_SkinnedVertices.at(i).TransformedVertex.Position, newPos);
		}
	}

	m_pMeshDrawer->RemoveTriangles();

	
	for (UINT i = 0; i < m_SkinnedVertices.size(); i+=4){
		QuadPosNormCol temp;
		temp.Vertex1 = m_SkinnedVertices.at(i).TransformedVertex;
		temp.Vertex2 = m_SkinnedVertices.at(i+1).TransformedVertex;
		temp.Vertex3 = m_SkinnedVertices.at(i+2).TransformedVertex;
		temp.Vertex4 = m_SkinnedVertices.at(i+3).TransformedVertex;

		m_pMeshDrawer->AddQuad(temp);
		
	}
	m_pMeshDrawer->UpdateBuffer();
}

void SoftwareSkinScene_2::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void SoftwareSkinScene_2::CreateMesh(){
	auto pos = XMFLOAT3(7.5f, 0, 0);
	auto offset = XMFLOAT3(7.5f, 2.5f, 2.5f);
	auto col = XMFLOAT4(1, 0, 0, 0.5f);
	//*****
	//BOX1*
	//*****
	//FRONT
	auto norm = XMFLOAT3(0, 0, -1);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	//BACK
	norm = XMFLOAT3(0, 0, 1);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//TOP
	norm = XMFLOAT3(0, 1, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	//BOTTOM
	norm = XMFLOAT3(0, -1, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//LEFT
	norm = XMFLOAT3(-1, 0, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));

	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//RIGHT
	norm = XMFLOAT3(1, 0, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	//*****
	//BOX2*
	//*****
	col = XMFLOAT4(0, 1, 0, 0.5f);
	pos = XMFLOAT3(22.5f, 0, 0);
	//FRONT
	norm = XMFLOAT3(0, 0, -1);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	//BACK
	norm = XMFLOAT3(0, 0, 1);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//TOP
	norm = XMFLOAT3(0, 1, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	//BOTTOM
	norm = XMFLOAT3(0, -1, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));

	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//LEFT
	norm = XMFLOAT3(-1, 0, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(-offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	//RIGHT
	norm = XMFLOAT3(1, 0, 0);
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, -offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, offset.z + pos.z), norm, col));
	m_SkinnedVertices.push_back(SkinnedVertex(XMFLOAT3(offset.x + pos.x, -offset.y +
		pos.y, -offset.z + pos.z), norm, col));
}
