//------------------------------------------------------------------------------------------------------
//   _____     _______ ____  _     ___  ____  ____    _____ _   _  ____ ___ _   _ _____   ______  ___ _ 
//  / _ \ \   / / ____|  _ \| |   / _ \|  _ \|  _ \  | ____| \ | |/ ___|_ _| \ | | ____| |  _ \ \/ / / |
// | | | \ \ / /|  _| | |_) | |  | | | | |_) | | | | |  _| |  \| | |  _ | ||  \| |  _|   | | | \  /| | |
// | |_| |\ V / | |___|  _ <| |__| |_| |  _ <| |_| | | |___| |\  | |_| || || |\  | |___  | |_| /  \| | |
//  \___/  \_/  |_____|_| \_\_____\___/|_| \_\____/  |_____|_| \_|\____|___|_| \_|_____| |____/_/\_\_|_|
//
// Overlord Engine v1.121
// Copyright Overlord Thomas Goussaert & Overlord Brecht Kets
// http://www.digitalartsandentertainment.com/
//------------------------------------------------------------------------------------------------------
//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"
#include "SkyboxScene.h"
#include "../../../OverlordEngine/Prefabs/SkyBoxPrefab.h"

SkyBoxScene::SkyBoxScene(void) :
	GameScene(L"SkyBoxScene")
{
}


SkyBoxScene::~SkyBoxScene(void)
{
}

void SkyBoxScene::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	auto skyBox = new SkyBoxPrefab(L"Resources/Textures/SkyBox_Nebula.dds"); //SkyBox_Clouds //SkyBox_Default //SkyBox_Nebula //SkyBox
	AddChild(skyBox);
}

void SkyBoxScene::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

}

void SkyBoxScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}
