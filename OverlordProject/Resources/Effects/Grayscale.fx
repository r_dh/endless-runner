//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Mirror;
    AddressV = Mirror;
};

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState depthStencil 
{
	DepthEnable = true;
	//DepthWriteMask = 1;
};
/// Create Rasterizer State (Backface culling) 
RasterizerState backCullingState
{
	CullMode = BACK;
};

Texture2D gTexture;

struct VS_INPUT_STRUCT
{
	float3 Pos : POSITION;
    float2 Tex : TEXCOORD;

};
struct PS_INPUT_STRUCT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD1;

};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT_STRUCT VS( VS_INPUT_STRUCT input )
{
	PS_INPUT_STRUCT output = (PS_INPUT_STRUCT)0;
	// Set the Position
	output.Pos = normalize(float4(input.Pos, 1.0f));
	// Set the Text coord
	output.Tex = input.Tex;

	return output;
}
//--------------------------------------------------------------------------------------
// Pixel XMeshShader
//--------------------------------------------------------------------------------------
float4 PS( PS_INPUT_STRUCT input): SV_Target
{
    // Step 1: sample the texture
	float4 textureSample = gTexture.Sample(samLinear, input.Tex);
	// Step 2: calculate the mean value
	float color = (textureSample.r + textureSample.g + textureSample.b) / 3.0f;
	// Step 3: return the color
	return float4(color, color, color, 1.0f);
}

technique11 Grayscale
{
    pass P0
    {          
        // Set states
		SetDepthStencilState(depthStencil, 0);
		SetRasterizerState(backCullingState);
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}

