TextureCube m_CubeMap : CubeMap;
matrix matWorldViewProj : WorldViewProjection;

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VS_IN
{
	float3 posL : POSITION;
};

struct VS_OUT
{
	float4 posH : SV_POSITION;
	float3 texC : TEXCOORD;
};

RasterizerState NoCull
{
	CullMode = NONE;
};

DepthStencilState LessEqualDSS
{
	// Make sure the depth function is LESS_EQUAL and not just LESS.
    // Otherwise, the normalized depth values at z = 1 (NDC) will
    // fail the depth test if the depth buffer was cleared to 1.
	
	//4. Set DepthFunc to Less Equal
	DepthFunc = LESS_EQUAL;

};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUT VS( VS_IN vIn )
{
	VS_OUT vOut = (VS_OUT)0;

	// 1.
	//Calculate posH
	//Set z = w so that z/w = 1 (effect: skydome always on far plane).
	vOut.posH.z = vOut.posH.w;
	vOut.posH = mul(float4(vIn.posL, 0.0f), matWorldViewProj).xyww;
	//2.
	// Set tex coord
	vOut.texC = vIn.posL;
	// Use local vertex position as Cubemap lookup vector (UVW)
	
	return vOut;
}
//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VS_OUT pIn): SV_Target
{
	//3. Sample Cubemap
	float4 color = m_CubeMap.Sample(samLinear, pIn.texC);
	return color;
}


technique11 Render
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
		//5. Set RasterizerState and DepthStencilState
		SetRasterizerState(NoCull);
		SetDepthStencilState(LessEqualDSS, 0);
    }
}