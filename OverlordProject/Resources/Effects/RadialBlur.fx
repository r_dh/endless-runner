Texture2D gTexture;

//sampler TextureSampler : register(s0);

SamplerState g_samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};
float2 Center = { 0.5, 0.5 }; //center of the screen
float BlurStart = 0.2f; // blur offset
float BlurWidth = -0.1; //how big it should be
int nsamples = 4;


float4 PS(float2 UV	: TEXCOORD0) : SV_TARGET
{
	return float4(0, 0, 0, 1.0f);
	/*
	UV -= Center;
	float4 c = 0;
		for (int i = 0; i < nsamples; i++) {
		float scale = BlurStart + BlurWidth*(i / (float)(nsamples - 1));
		//c += tex2D(TextureSampler, UV * scale + Center);
		c += gTexture.Sample(g_samLinear, UV * scale + Center);
		}
	c /= nsamples;
	return c;*/


/*	UV -= Center;
	float2 colorvalue = UV;// abs(UV - Center);

	if (colorvalue.x > colorvalue.y) {
		colorvalue.x = colorvalue.y
	}

	c = tex2D(TextureSampler, UV);

	float color = (textureSample.r + textureSample.g + textureSample.b) / 3.0f;
	// Step 3: return the color
	return float4(color, color, color, 1.0f);

	return c;// *float4(0.2f, 0.2f, 0.2f, 1.f);*/
}

technique11 RadialBlur
{
    pass P0
    {
		// Set states
      // SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}