//#pragma enable_d3d11_debug_symbols
//************
// VARIABLES *
//************
cbuffer cbPerObject
{
	float4x4 m_MatrixWorldViewProj : WORLDVIEWPROJECTION;
	float4x4 m_MatrixWorld : WORLD;
	float3 m_LightDir = { 0.2f, -1.0f, 0.2f };
	float2 m_SubDivisions = { 16.f, 16.f }; //16 16 / 4 4
}

RasterizerState FrontCulling
{
	CullMode = NONE;
};

SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Border;//Wrap of Mirror of Clamp of Border
	AddressV = Border;//Wrap of Mirror of Clamp of Border
};

Texture2D m_TextureDiffuse, m_TextureNoise;

//**********
// STRUCTS *
//**********
struct VS_DATA
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD;
};

struct GS_DATA
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 TexCoord : TEXCOORD0;
	float Height : HEIGHT;
};

//****************
// VERTEX SHADER *
//****************
VS_DATA MainVS(VS_DATA vsData) {

	return vsData;
}

//******************
// GEOMETRY SHADER *
//******************
void CreateVertex(inout TriangleStream<GS_DATA> triStream, float3 pos, float3 normal, float heightVal, float2 texCoord)     //, float cubeHeight
{
	//Step 1. Create a GS_DATA object
	GS_DATA tri;
	//Step 2. Transform the position using the WVP Matrix
	tri.Position = mul(float4(pos, 1.0f), m_MatrixWorldViewProj);
	//Step 3. Transform the normal using the World Matrix
	tri.Normal = mul(normal, (float3x3)m_MatrixWorld);
	//Step 4. Assign texCoord to (GS_DATA object).TexCoord
	tri.TexCoord = texCoord;
	//Step 5. Assign heightValue
	tri.Height = heightVal;
	//Step 6. Append (GS_DATA object) to the TriangleStream parameter
	triStream.Append(tri);
}

void CreateQuad(inout TriangleStream<GS_DATA> triStream, float3 quad[4], float3 normal, float heightVal, float2 texCoord[4] = { float2(0, 0), float2(1, 0), float2(0, 1), float2(1, 1) }) {

	CreateVertex(triStream, quad[0], normal, heightVal, texCoord[0]); //left top
	CreateVertex(triStream, quad[1], normal, heightVal, texCoord[1]); //right top
	CreateVertex(triStream, quad[2], normal, heightVal, texCoord[2]); //left bottom
	CreateVertex(triStream, quad[3], normal, heightVal, texCoord[3]); //right bottom

	triStream.RestartStrip();
}

[maxvertexcount(96)] //currenty 4 cubes with 6 sides with 4 vertices for each side: 4 vertices * 6 sides = 24; 24 vertices per cube * 4 cubes = 96 //with (maxvertexcount * 10) < 1024 
void CubeGenerator(triangle VS_DATA vertices[3], inout TriangleStream<GS_DATA> triStream) {

	//Step 1: Find 2 right sides of the triangle and eliminate the hypotenuse
	//- Find width
	float widthLength = vertices[1].Position.x - vertices[0].Position.x;
	if ((vertices[2].Position.x - vertices[0].Position.x) > widthLength) widthLength = vertices[2].Position.x - vertices[0].Position.x;
	//- Find depth
	float depthLength = vertices[1].Position.y - vertices[0].Position.y;
	if (depthLength == 0) {
		depthLength = vertices[2].Position.y - vertices[0].Position.y;
	}

	//Step 2: Subdivide triangle and create ground quads
	float widthSubDiv = 2, depthSubDiv = 2; //max 2 * 2 because the geometry can't handle too many vertices as output //each iteration is 24 extra
	float quadWidth = widthLength / widthSubDiv,
		quadDepth = depthLength / depthSubDiv;

	float heightCube = 0;

	if (widthLength > 0) {
		for (int i = 0; i < (int)widthSubDiv; ++i) {
			for (int j = 0; j < (int)depthSubDiv; ++j) {

				//heightCube = -abs(quadDepth) * j + i * -abs(quadWidth);

				//Divide triangle into smaller rectangles
				float3 groundPlane[4];
				groundPlane[0] = vertices[0].Position + float3(i * quadWidth, j * quadDepth, 0);
				groundPlane[1] = vertices[0].Position + float3((i + 1) * quadWidth, j * quadDepth, 0);
				groundPlane[2] = vertices[0].Position + float3(i * quadWidth, (j + 1) * quadDepth, 0);
				groundPlane[3] = vertices[0].Position + float3((i + 1) * quadWidth, (j + 1) * quadDepth, 0);


				//Get normalized position of new quad to sample from noise map.
				//Get world position
				float3 worldPosition = (groundPlane[0] + groundPlane[1] + groundPlane[2] + groundPlane[3]) / 4;
				//Normalize position to 0 - 1 range
				float2 normalizedPosition;
				//Length of a triangle times triangles in half plane to get halfWidth and halfHeight
				float halfWidth = widthLength * (m_SubDivisions.x / 2), halfHeight = depthLength * (m_SubDivisions.y / 2);
				//Add halfWidth and halfHeight to worldposition quad to push it into positive numbers and divide by total length of the side to normalize 0 - 1
				normalizedPosition.x = (halfWidth + worldPosition.x) / (halfWidth * 2);
				normalizedPosition.y = (halfHeight - worldPosition.y) / (halfHeight * 2);

				//Sample the color and calculate mean value
				float3 color = m_TextureNoise.SampleGrad(samLinear, normalizedPosition, float2(0, 0), float2(0, 0)).rgb;
				float value = (color.x + color.y + color.z) / 3;

				//Bottom
				float2 texCoord[4] = { float2(1, 1), float2(0, 1), float2(1, 0), float2(0, 0) };
				CreateQuad(triStream, groundPlane, vertices[0].Normal, value, texCoord);

				heightCube = -100 * value;
				if (heightCube < -0.00001f) { //optimization for later
					//Top
					float3 topPlane[4];
					topPlane[0] = groundPlane[0] + float3(0, 0, heightCube);
					topPlane[1] = groundPlane[1] + float3(0, 0, heightCube);
					topPlane[2] = groundPlane[2] + float3(0, 0, heightCube);
					topPlane[3] = groundPlane[3] + float3(0, 0, heightCube);

					float2 texCoordTop[4] = { float2(0, 1), float2(1, 1), float2(0, 0), float2(1, 0) };
					CreateQuad(triStream, topPlane, vertices[0].Normal, value, texCoordTop);

					//Right
					float3 rightPlane[4];
					rightPlane[0] = topPlane[1];
					rightPlane[1] = topPlane[3];
					rightPlane[2] = groundPlane[1];
					rightPlane[3] = groundPlane[3];

					CreateQuad(triStream, rightPlane, float3(0, 0, -1), value);

					//Left
					float3 leftPlane[4];
					leftPlane[0] = topPlane[2];
					leftPlane[1] = topPlane[0];
					leftPlane[2] = groundPlane[2];
					leftPlane[3] = groundPlane[0];

					CreateQuad(triStream, leftPlane, float3(0, 0, -1), value);

					//Front
					float3 frontPlane[4];
					frontPlane[0] = topPlane[0];
					frontPlane[1] = topPlane[1];
					frontPlane[2] = groundPlane[0];
					frontPlane[3] = groundPlane[1];

					CreateQuad(triStream, frontPlane, float3(0, 0, -1), value);

					//Back
					float3 backPlane[4];
					backPlane[0] = topPlane[3];
					backPlane[1] = topPlane[2];
					backPlane[2] = groundPlane[3];
					backPlane[3] = groundPlane[2];

					CreateQuad(triStream, backPlane, float3(0, 0, -1), value);
				}
			}
		}
	}
}

//***************
// PIXEL SHADER *
//***************
float4 MainPS(GS_DATA input) : SV_TARGET
{
	input.Normal = -normalize(input.Normal);
	float alpha = m_TextureDiffuse.Sample(samLinear, input.TexCoord).a;

	float s = max(dot(m_LightDir, input.Normal), 0.4f);
	
	float3 color;
	if (input.Height < 0.000001f) color = float3(0, 0.4f, 1); //blue sea level
		else if ((input.Height - 0.1f) < 0.0001f) color = lerp(float3(0.f, 0.5, 0), float3(0.2, 0.2, 0), 0.8 - input.Height * 9);
		else if ((input.Height - 0.2f) < 0.0001f) color = lerp(float3(0.4f, 0.4f, 0), float3(0.9f, 1.0f, 0), 1 - input.Height * 4);
		else if ((input.Height - 0.3f) < 0.0001f) color = lerp(float3(1, 1, 0), float3(1, 0.5f, 0),  1 - input.Height); 
		else if ((input.Height - 0.4f) < 0.0001f) color = lerp(float3(1, 0.5, 0), float3(1, 0.2f, 0), 1 - input.Height); 
		else if ((input.Height - 0.5f) < 0.0001f) color = float3(1, 0.75f, 0);
		else if ((input.Height - 0.9f) < 0.0001f) color = float3(1, 0.15f, 0);
		else {
			color = lerp(float3(0.f, 0.5, 0), float3(1, 0.2, 0), input.Height);
		}

		return float4(color*s, alpha);
	//color value
	//blue:		   0 ; 0.5     ; 1
	//green	 0 - 0.5 ; 1       ; 0
	//orange       1 ; 0.5 - 1 ; 0
	//red          1 ; 0       ; 0



	//R
	//heightval
	//G
	//0.5 + (1 - heightval) * heightval // ceil(heightval - 0.00001f) == 0 ? ; 0.5 : 1 - heightval
	//B
	// 1 - ceil(heightval - 0.00001f)

	return float4(color*s, alpha);
}


//*************
// TECHNIQUES *
//*************
technique10 DefaultTechnique
{
	pass p0 {
		SetRasterizerState(FrontCulling);
		SetVertexShader(CompileShader(vs_4_0, MainVS()));
		SetGeometryShader(CompileShader(gs_4_0, CubeGenerator()));
		SetPixelShader(CompileShader(ps_4_0, MainPS()));
	}
}