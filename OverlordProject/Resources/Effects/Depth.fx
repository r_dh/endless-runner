//=============================================================================
//// Shader uses position and texture
//=============================================================================
SamplerState samLinear
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap; //Clamp //Mirror
	AddressV = Wrap;
};

/// Create Depth Stencil State (ENABLE DEPTH WRITING)
DepthStencilState depthStencil
{
	DepthEnable = true;
	DepthWriteMask = 1;
};
/// Create Rasterizer State (Backface culling) 
RasterizerState backCullingState
{
	CullMode = BACK;
};

Texture2D gTexture;

struct VS_INPUT_STRUCT
{
	float3 Pos : POSITION;
	float4 Tex : TEXCOORD;

};
struct PS_INPUT_STRUCT
{
	float4 Pos : SV_POSITION;
	float4 Tex : TEXCOORD1;
	float Depth : DEPTH0;

};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT_STRUCT VS(VS_INPUT_STRUCT input) {
	PS_INPUT_STRUCT output = (PS_INPUT_STRUCT)0;
	// Set the Position
	//output.Depth = input.Pos.w;	
	output.Pos = normalize(float4(input.Pos, 1.0f));
	//output.Depth = input.Pos.z / output.Pos.w;
	// Set the Text coord
	output.Tex = input.Tex;

	return output;
}
//--------------------------------------------------------------------------------------
// Pixel XMeshShader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT_STRUCT input) : SV_Target
{
	//Mirror shader

	float4 textureSample = gTexture.Sample(samLinear, input.Tex);

	float3 color = textureSample;
	float depth = textureSample.w;

	float2 offset = input.Tex + float2(0, -0.65f); //1.2f
	offset = saturate(offset);
	float3 colorDiff = gTexture.Sample(samLinear, offset); //input.Tex - 0.5f
	float2 center = input.Tex - float2(0.5f, 0.5f);
	center = abs(center);
	if (center.x > center.y) center.x = center.y;
	color -= colorDiff / 1.5f; //++denominator == --intensity
	return float4(color, depth);

}

technique11 Depth
{
	pass P0
	{
		// Set states
		//SetDepthStencilState(depthStencil, 1);
		//SetRasterizerState(backCullingState);
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}

