//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "PostDepth.h"
#include "Base/GeneralStructs.h"
#include "Diagnostics/Logger.h"
#include "Content/ContentManager.h"
#include "Graphics/TextureData.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostDepth::m_pDiffuseSRVvariable = nullptr;

PostDepth::PostDepth() : PostProcessingMaterial(L"./Resources/Effects/Depth.fx", 0, L"Depth") {
}


PostDepth::~PostDepth() {

}

void PostDepth::LoadEffectVariables() {
	if (!m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
		if (!m_pDiffuseSRVvariable->IsValid()) {
			Logger::LogWarning(L"PostDepth::LoadEffectVariables() > \'gTexture\' variable not found!");
			m_pDiffuseSRVvariable = nullptr;
		}
	}
}

void PostDepth::UpdateEffectVariables(RenderTarget* rendertarget) {
	if (m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable->SetResource(rendertarget->GetShaderResourceView());
	}
}