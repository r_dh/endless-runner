#pragma once
#include "Graphics/Material.h"
#include "Content/ContentManager.h"

//using namespace std;
class TextureData;

class DiffuseMaterial : public Material
{
public:
	DiffuseMaterial();
	~DiffuseMaterial();

	void SetDiffuseTexture(const wstring &assetFile);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent);

private:
	TextureData* m_pDiffuseTexture;
	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

	DiffuseMaterial(const DiffuseMaterial &obj);
	DiffuseMaterial& operator=(const DiffuseMaterial &obj);
};
