#pragma once
#include "Graphics\PostProcessingMaterial.h"

class TextureData;

class PostDepth : public PostProcessingMaterial
{
public:
	PostDepth();
	~PostDepth();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostDepth(const PostDepth &obj);
	PostDepth& operator=(const PostDepth& obj);
};

