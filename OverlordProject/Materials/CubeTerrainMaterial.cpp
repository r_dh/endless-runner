//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "CubeTerrainMaterial.h"
#include "Base/GeneralStructs.h"
#include "Diagnostics/Logger.h"
#include "Content/ContentManager.h"
#include "Graphics/TextureData.h"

ID3DX11EffectShaderResourceVariable* CubeTerrainMaterial::m_pDiffuseSRVvariable = nullptr;
ID3DX11EffectShaderResourceVariable* CubeTerrainMaterial::m_pNoiseSRVvariable = nullptr;

CubeTerrainMaterial::CubeTerrainMaterial() : Material(L"./Resources/Effects/CubeShader.fx"),
m_pDiffuseTexture(nullptr),
m_pNoiseTexture(nullptr),
m_ZoomLevelEffectVar(nullptr) {
}

CubeTerrainMaterial::~CubeTerrainMaterial() {

}

void CubeTerrainMaterial::SetDiffuseTexture(const wstring& assetFile) {
	m_pDiffuseTexture = ContentManager::Load<TextureData>(assetFile);
}

void CubeTerrainMaterial::SetNoiseTexture(const wstring& assetFile) {
	m_pNoiseTexture = ContentManager::Load<TextureData>(assetFile);
}

void CubeTerrainMaterial::SetZoomLevel(const float zoom) {
	m_ZoomLevel = zoom;
}

void CubeTerrainMaterial::LoadEffectVariables() {
	if (!m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable = m_pEffect->GetVariableByName("m_TextureDiffuse")->AsShaderResource();
		if (!m_pDiffuseSRVvariable->IsValid()) {
			Logger::LogWarning(L"CubeTerrainMaterial::LoadEffectVariables() > \'m_TextureDiffuse\' variable not found!");
			m_pDiffuseSRVvariable = nullptr;
		}
	}

	if (!m_pNoiseSRVvariable) {
		m_pNoiseSRVvariable = m_pEffect->GetVariableByName("m_TextureNoise")->AsShaderResource();
		if (!m_pNoiseSRVvariable->IsValid()) {
			Logger::LogWarning(L"CubeTerrainMaterial::LoadEffectVariables() > \'m_TextureNoise\' variable not found!");
			m_pNoiseSRVvariable = nullptr;
		}
	}

	m_ZoomLevelEffectVar = m_pEffect->GetVariableByName("m_SubDivisions")->AsVector();
	;
}

void CubeTerrainMaterial::UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent) {
	UNREFERENCED_PARAMETER(gameContext);
	UNREFERENCED_PARAMETER(pModelComponent);

	if (m_pDiffuseTexture && m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable->SetResource(m_pDiffuseTexture->GetShaderResourceView());
	}

	if (m_pNoiseTexture && m_pNoiseSRVvariable) {
		m_pNoiseSRVvariable->SetResource(m_pNoiseTexture->GetShaderResourceView());
	}

	XMFLOAT2 zoom = XMFLOAT2(m_ZoomLevel, m_ZoomLevel);
	m_ZoomLevelEffectVar->SetFloatVector(reinterpret_cast<float*>(&zoom));
}