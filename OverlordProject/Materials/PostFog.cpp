//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "PostFog.h"
#include "Base/GeneralStructs.h"
#include "Diagnostics/Logger.h"
#include "Content/ContentManager.h"
#include "Graphics/TextureData.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostFog::m_pDiffuseSRVvariable = nullptr;

PostFog::PostFog() : PostProcessingMaterial(L"./Resources/Effects/FogShader.fx", 0, L"FogShader") {
}


PostFog::~PostFog() {

}

void PostFog::LoadEffectVariables() {
	if (!m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
		if (!m_pDiffuseSRVvariable->IsValid()) {
			Logger::LogWarning(L"PostFog::LoadEffectVariables() > \'gTexture\' variable not found!");
			m_pDiffuseSRVvariable = nullptr;
		}
	}
}

void PostFog::UpdateEffectVariables(RenderTarget* rendertarget) {
	if (m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable->SetResource(rendertarget->GetShaderResourceView());
	}
}