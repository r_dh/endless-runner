#pragma once
#include "Graphics\PostProcessingMaterial.h"

class TextureData;

class PostBlur : public PostProcessingMaterial
{
public:
	PostBlur();
	~PostBlur();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostBlur(const PostBlur &obj);
	PostBlur& operator=(const PostBlur& obj);
};

