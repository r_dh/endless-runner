#pragma once
#include "Graphics\PostProcessingMaterial.h"

class PostGrayScale : public PostProcessingMaterial
{
public:
	PostGrayScale();
	~PostGrayScale();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostGrayScale(const PostGrayScale &obj);
	PostGrayScale& operator=(const PostGrayScale& obj);
};

