#pragma once
#include "Graphics\Material.h"

class TextureData;

class CubeTerrainMaterial : public Material
{
public:
	CubeTerrainMaterial();
	~CubeTerrainMaterial();

	void SetDiffuseTexture(const wstring& assetFile);
	void SetNoiseTexture(const wstring& assetFile);
	void SetZoomLevel(const float zoom);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent);

	TextureData *m_pDiffuseTexture, *m_pNoiseTexture;
	static ID3DX11EffectShaderResourceVariable *m_pDiffuseSRVvariable;
	static ID3DX11EffectShaderResourceVariable *m_pNoiseSRVvariable;
	ID3DX11EffectVectorVariable *m_ZoomLevelEffectVar;

private:
	float m_ZoomLevel;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	CubeTerrainMaterial(const CubeTerrainMaterial &obj);
	CubeTerrainMaterial& operator=(const CubeTerrainMaterial& obj);
};

