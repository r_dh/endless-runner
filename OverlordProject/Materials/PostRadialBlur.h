#pragma once
#include "Graphics\PostProcessingMaterial.h"

class TextureData;

class PostRadialBlur : public PostProcessingMaterial
{
public:
	PostRadialBlur();
	~PostRadialBlur();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostRadialBlur(const PostRadialBlur &obj);
	PostRadialBlur& operator=(const PostRadialBlur& obj);
};

