//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "Base\stdafx.h"

#include "PostRadialBlur.h"
#include "Base/GeneralStructs.h"
#include "Diagnostics/Logger.h"
#include "Content/ContentManager.h"
#include "Graphics/TextureData.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostRadialBlur::m_pDiffuseSRVvariable = nullptr;

PostRadialBlur::PostRadialBlur() : PostProcessingMaterial(L"./Resources/Effects/RadialBlur.fx", 0, L"RadialBlur") {
}


PostRadialBlur::~PostRadialBlur() {

}

void PostRadialBlur::LoadEffectVariables() {
	if (!m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
		if (!m_pDiffuseSRVvariable->IsValid()) {
			Logger::LogWarning(L"PostRadialBlur::LoadEffectVariables() > \'gTexture\' variable not found!");
			m_pDiffuseSRVvariable = nullptr;
		}
	}
}

void PostRadialBlur::UpdateEffectVariables(RenderTarget* rendertarget) {
	if (m_pDiffuseSRVvariable) {
		m_pDiffuseSRVvariable->SetResource(rendertarget->GetShaderResourceView());
	}
}