#pragma once
#include "Graphics/Material.h"
#include "Content/ContentManager.h"

//using namespace std;
class TextureData;

class BackGroundMaterial : public Material
{
public:
	BackGroundMaterial();
	~BackGroundMaterial();

	void SetDiffuseTexture(const wstring &assetFile);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent);

private:
	TextureData* m_pDiffuseTexture;
	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

	BackGroundMaterial(const BackGroundMaterial &obj);
	BackGroundMaterial& operator=(const BackGroundMaterial &obj);
};
