#pragma once
#include "Graphics\PostProcessingMaterial.h"

class TextureData;

class PostFog : public PostProcessingMaterial
{
public:
	PostFog();
	~PostFog();

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable* m_pDiffuseSRVvariable;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostFog(const PostFog &obj);
	PostFog& operator=(const PostFog& obj);
};

